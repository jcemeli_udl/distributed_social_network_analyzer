# -*- coding: utf-8 -*-
import os
import sys

if __name__ == '__main__' :
	with open(sys.argv[1],'r') as conversations:
		for conversation in conversations:
			conversation = conversation.strip("\n")
			print "Starting conversation: " + conversation
			try:
				cmd = "python Downloaders/twitter-collector/twcollect.py " + conversation
				os.system(cmd)
			except:
				print "Error on conversation " + conversation
			print "Finished conversation " + conversation
