# -*- coding: utf-8 -*-
import os
import sys

if __name__ == '__main__' :
    with open(sys.argv[1],'r') as conversations:
        for conversation in conversations:
            conversation = conversation.strip("\n")
            print "Parsing conversation: " + conversation
            try:
                cmd = "python parser/parser_all.py -i " + conversation
                os.system(cmd)
            except:
                print "Error on conversation " + conversation
            print "Finished conversation " + conversation
