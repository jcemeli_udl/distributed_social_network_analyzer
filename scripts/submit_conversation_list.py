# -*- coding: utf-8 -*-
import os
import sys

if __name__ == '__main__' :
    with open(sys.argv[1],'r') as conversations:
        for conversation in conversations:
            conversation = conversation.strip("\n")
            cmd = "python scripts/analize_conversation.py -i " + conversation 
            os.system(cmd)
            print "Finished conversation " + conversation + "\n"
