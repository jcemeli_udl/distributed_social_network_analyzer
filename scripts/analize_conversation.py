# -*- coding: utf-8 -*-
import os
import sys
import argparse
import os.path

if __name__ == '__main__' :

	# Parse arguments
	parser = argparse.ArgumentParser(description = 'No')

	parser.add_argument('-d', '--download', default=False, help="Force to download conversation", action='store_true', dest = 'download')
	parser.add_argument('-np', '--no-parse', default=True, help="Force to parse", action='store_false', dest = 'parse')

	parser.add_argument('-i', '--id', type = str, help = 'ID Conversation', dest = 'id_conversation', required=True)
	parser.add_argument('-dem', '--deploy-mode', type = str, default=None, help = 'Deploy mode', dest = 'deploy_mode')
	parser.add_argument('-em', '--executor-memory', type = str, default=None, help = 'Executor Memory', dest = 'executor_memory',)
	parser.add_argument('-dm', '--driver-memory', type = str, default=None, help = 'Driver Memory', dest = 'driver_memory')
	parser.add_argument('-dc', '--driver-cores', type = str, default=None, help = 'Driver Cores', dest = 'driver_cores')

	args = parser.parse_args()

	# Download Conversation if required
	if args.download:
		cmd = "python Downloaders/twitter-collector/twcollect.py " + args.id_conversation
		os.system(cmd)

	else:
		json_path = "Storage/Jsons/" + args.id_conversation + ".json"
		# Conversation not on filesystem -> Download
		try:
			json_conversation = open(json_path,'r')
			print "Conversation Found"
		except:
			print "Downloading Conversation"
			cmd = "python Downloaders/twitter-collector/twcollect.py " + args.id_conversation
			os.system(cmd)

	# Parser
	if args.parse:
		print "Parsing Conversation"
		cmd = "python parser/parser_all.py -i " + args.id_conversation
		os.system(cmd)

		# Upload Files to Hadoop [Needs to be started]
		print "Uploading Files to Hadoop"
		cmd = "../hadoop-2.7.3/bin/hdfs dfs -put -f Storage/Files /user/scala-cluster/Storage"
		os.system(cmd)
	else:
		print "No parsing and Uploading to Hadoop, make sure the files are allocated there"

	# Spark spark-submit
	print "Submiting to spark-cluster"
	cmd = "../spark-2.1.0-bin-hadoop2.7/bin/spark-submit --class 'MainApp' "

	if args.deploy_mode:
		if args.deploy_mode == "cluster":
			print "Deploying as cluster"
			cmd = cmd + "--master spark://udlnet-05-108.udl.net:6066 --deploy-mode cluster "
	else:
		print "Deploying as client [default]"
		cmd = cmd + "--master spark://udlnet-05-108.udl.net:7077 "
	if args.executor_memory:
		print "Executor Memory: " + args.executor_memory
		cmd = cmd + "--executor-memory " + args.executor_memory + " "
	if args.driver_memory:
		print "Driver Memory: " + args.driver_memory
		cmd = cmd + "--driver-memory " + args.driver_memory + " "
	if args.driver_cores:
		print "Driver Cores: " + args.driver_cores
		cmd = cmd + "--driver-cores " + args.driver_cores + " "
	cmd = cmd + "target/scala-2.11/social-network-analyzer_2.11-1.0.jar " + args.id_conversation + " > Storage/Output/" + args.id_conversation + \
		".txt 2> Storage/Output/" + args.id_conversation + "-error.txt"
	os.system(cmd)
	print "Finished!"
