# Distributed Social Network Analyzer #

### Requirements ###

Required sbt version: 0.13.13

You can get help [here](http://www.scala-sbt.org/download.html)



### Compiling ###

Go to the project folder and execute
```
$sbt package
```

### Executing ###

The main way to execute the project in Apache Spark after compiling:


```
$spark-submit --class "AppName" --master local[#nodes] target/(path-to-jar)/AppName.jar
```

Execution example for this project:

```
$spark-submit --class "MainApp" --master local[4] target/scala-2.11/social-network-analyzer_2.11-1.0.jar
```