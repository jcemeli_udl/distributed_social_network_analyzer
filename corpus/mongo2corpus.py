#!/usr/bin/python -ttOO
# -*- coding: utf-8 -*-

import sys
import re
import pymongo
import nltk

import corpusutils.corpusutils as CUtils



"""  https://docs.python.org/2/library/re.html  """


if __name__ == "__main__":
     """
       Program for creating feature sets from dictionaries,
       adding to them any other features not present on the
       base dictionaries

     """    

     tweetsdb = sys.argv[1]
     tweetscollection = sys.argv[2]
     limitsize = int(sys.argv[3])
     excludestopwords = bool(int(sys.argv[4]))
     stopwordsafter = int(sys.argv[5])
     lang = sys.argv[6]

     connection = pymongo.MongoClient('%s://%s:%i/' % ('mongodb', 'localhost', 27017))
     db = connection[tweetsdb]
     collection = db[tweetscollection]

     stopwords = nltk.corpus.stopwords.words(lang)
     dic = {}
     for post in collection.find():
	   for w in re.split( '[\s,]+' , post['text'] ):
                wf = CUtils.transform_word(w) 
                # print wf
                if (len(wf) > 0):
                  subwords = CUtils.extractsubwords(wf)                     
                else:
                  subwords = list()
                # print len(subwords)
                for sb in subwords: 
                  # lsb = sb.lower()
                  # print sb
                  if (len(sb) > 0):
                    if (excludestopwords):
    		      if (sb not in stopwords):
		  	dic[sb] = dic.get( sb, 0 ) + 1
                    else:
		      dic[sb] = dic.get( sb, 0 ) + 1

     numwords = 0
     # print len(dic)
     for w in sorted( dic, key=dic.get, reverse=True ):
        if (numwords < limitsize):
          print w.encode('utf8'), ' : ', dic[w]
          numwords += 1
        else:
          break 
     if (excludestopwords and (stopwordsafter > 0)):
        for sword in stopwords[0:stopwordsafter]:
           print sword.encode('utf8'), ' : ', 1


