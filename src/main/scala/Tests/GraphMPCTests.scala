//SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.DataFrame
import scala.collection.mutable.ListBuffer
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX TODO: TEST ONLY
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

//Read files
import scala.io.Source
import sys.process._
import java.io._

class GraphMPCTests (session: SparkSession){

  def listTriplets(id: String){
    val downloader = new TwitterDownloader(session.sparkContext)
    val path = downloader.downloadAndStoreConversation(id)

    //Create MPC
    val parserCaller = new ParserCaller(session)
    val classifier = parserCaller.parseJson(path)
    //Create graph
    val testGraph = new TwitterGraph(session.sparkContext, classifier)
    testGraph.parseConversationFromFile(path)
    val graph = testGraph.getGraph()
    val facts: RDD[String] =
    graph.triplets.map(triplet =>
    triplet.srcAttr.author + " is " + triplet.attr + " with " + triplet.dstAttr.author)
    facts.collect.foreach(println(_))
  }

  /*
   Some conversations:
   835171254852202496

  */

}
