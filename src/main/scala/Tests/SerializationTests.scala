//SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX TODO: TEST ONLY
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

//Read files
import scala.io.Source

class SerializationTests(session: SparkSession){

  val context = session.sparkContext

  def graphSerailizingTest(){
    val jGraph = new SocialGraph(context)
    jGraph.addConnection(1, 2, "noone")
    jGraph.addConnection(1, 3, "noone")
    val someMap : Map[String, Any] = Map("Something" -> "Content")
    jGraph.addMessage(1, "author1", "content1", someMap)
    jGraph.addMessage(2, "author2", "content2", someMap)
    jGraph.addMessage(3, "author3", "content3", someMap)

    jGraph.makeGraph()

    val graph = jGraph.getGraph()

    val contentCount = graph.vertices.filter{
      case(id, message) => message.message == "content1"
    }.count

    val relationToRootCount = graph.edges.filter(e => e.srcId < e.dstId).count


    println("Total messages with content 1: " + contentCount + "\n" +
      "Total edges going to higher node id: " + relationToRootCount)
    jGraph.storeGraph("Storage/Graphs/", "Test1")

    val lGraph = new SocialGraph(context)
    lGraph.loadGraph("Storage/Graphs/", "Test1")
    lGraph.makeGraph()

    val loaded_graph = lGraph.getGraph()

    val loaded_contentCount = loaded_graph.vertices.filter{
      case(id, message) => message.message == "content1"
    }.count

    val loaded_relationToRootCount = loaded_graph.edges.filter(e => e.srcId < e.dstId).count


    println("[LOADED GRAPH]Total messages with content 1: " + loaded_contentCount + "\n" +
      "[LOADED GRAPH]Total edges going to higher node id: " + loaded_relationToRootCount)
  }

  def twitterStoringTest(){
    println("[SERIALIZATION_TEST]: Storing Twitter graph")
    val testGraph : TwitterGraph = getMultiReferenceConversationGraph()
    println("[SERIALIZATION_TEST]: Twitter graph downloaded, storging...")
    testGraph.storeGraph("Storage/Graphs/", "TwitterTest")
    println("[SERIALIZATION_TEST]: stored, loading from storage")
    val lGraph = new SocialGraph(context)
    lGraph.loadGraph("Storage/Graphs/", "TwitterTest")
    lGraph.makeGraph()

    println("Max distance of the graph is " + testGraph.maxDistance()
         +"\nMin distance of the graph is " + testGraph.minDistance()
         +"\nAverage distance is " + testGraph.averageDistance())
   println("Max distance of the LOADED graph is " + lGraph.maxDistance()
        +"\nMin distance of the LOADED graph is " + lGraph.minDistance()
        +"\nAverage distance LOADED is " + lGraph.averageDistance())
  }

  private def getMultiReferenceConversationGraph(): TwitterGraph = {
    storedTestGraph match {
      case Some(i) => i
      case None =>  {
        val downloader = new TwitterDownloader(session.sparkContext)
        val path = downloader.downloadAndStoreConversation("574324656905281538")
        //Create MPC
        val parserCaller = new ParserCaller(session)
        val classifier = parserCaller.parseJson(path)
        //Create graph
        val testGraph = new TwitterGraph(session.sparkContext, classifier)
        testGraph.parseConversationFromFile(path)
        storedTestGraph = Some(testGraph)
        testGraph
      }
    }
  }

  var storedTestGraph : Option[TwitterGraph] = None

}
