//SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX TODO: TEST ONLY
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

//Read files
import scala.io.Source
import sys.process._
import java.io._

class DistributedGraphTests (session: SparkSession){

  val context = session.sparkContext

  def listTriplets(){
    val testGraph : TwitterGraph = getMultiReferenceConversationGraph()
    val graph = testGraph.getGraph()
    val facts: RDD[String] =
    graph.triplets.map(triplet =>
    triplet.srcAttr.author + " is " + triplet.attr + " with " + triplet.dstAttr.author)
    facts.collect.foreach(println(_))
  }

  def twitterDistanceTest(){
    println("[DISTRIBUTED_TEST]: Pregel test")
    val testGraph : TwitterGraph = getMultiReferenceConversationGraph()
    println("Max distance of the graph is " + testGraph.maxDistance()
         +"\nMin distance of the graph is " + testGraph.minDistance()
         +"\nAverage distance is " + testGraph.averageDistance())
  }

  def distancesTest(){
    //Create a graph with min depth 2 and max depth 3
    val sGraph : SocialGraph = new SocialGraph(context)
    val someMap : Map[String, Any] = Map("Something" -> "Content")
    sGraph.addMessage(1, "author1", "content1", someMap)
    sGraph.addMessage(2, "author2", "content2", someMap)
    sGraph.addMessage(3, "author3", "content3", someMap)
    sGraph.addMessage(4, "author4", "content4", someMap)
    sGraph.addMessage(5, "author5", "content5", someMap)
    sGraph.addMessage(6, "author6", "content6", someMap)
    sGraph.addMessage(7, "author7", "content7", someMap)
    sGraph.addMessage(8, "author8", "content8", someMap)

    sGraph.addConnection(2, 1, "noone")
    sGraph.addConnection(3, 1, "noone")
    sGraph.addConnection(4, 2, "noone")
    sGraph.addConnection(5, 2, "noone")
    sGraph.addConnection(6, 3, "noone")
    sGraph.addConnection(7, 3, "noone")
    sGraph.addConnection(8, 5, "noone")

    println("Min distance of the graph is " + sGraph.minDistance()
         +"\nMax distance of the graph is " + sGraph.maxDistance()
         +"\nAverage distance is " + sGraph.averageDistance())
  }

  def distancesTestAlt(){
    //Create a graph with min depth 2 and max depth 3
    val sGraph : SocialGraph = new SocialGraph(context)
    val someMap : Map[String, Any] = Map("Something" -> "Content")
    sGraph.addMessage(1,  "author1",  "content1",  someMap)
    sGraph.addMessage(2,  "author2",  "content2",  someMap)
    sGraph.addMessage(3,  "author3",  "content3",  someMap)
    sGraph.addMessage(4,  "author4",  "content4",  someMap)
    sGraph.addMessage(5,  "author5",  "content5",  someMap)
    sGraph.addMessage(6,  "author6",  "content6",  someMap)
    sGraph.addMessage(7,  "author7",  "content7",  someMap)
    sGraph.addMessage(8,  "author8",  "content8",  someMap)
    sGraph.addMessage(9,  "author9",  "content9",  someMap)
    sGraph.addMessage(10, "author10", "content10", someMap)

    sGraph.addConnection(2,  1, "noone")
    sGraph.addConnection(3,  1, "noone")
    sGraph.addConnection(4,  2, "noone")
    sGraph.addConnection(5,  2, "noone")
    sGraph.addConnection(6,  2, "noone")
    sGraph.addConnection(7,  5, "noone")
    sGraph.addConnection(8,  5, "noone")
    sGraph.addConnection(9,  3, "noone")
    sGraph.addConnection(10, 3, "noone")


    println("Min distance of the graph is " + sGraph.minDistance()
         +"\nMax distance of the graph is " + sGraph.maxDistance()
         +"\nAverage distance is " + sGraph.averageDistance())
  }

  def defeatersCountTest(){
    //Create a graph with min depth 2 and max depth 3
    val sGraph : SocialGraph = new SocialGraph(context)
    val someMap : Map[String, Any] = Map("Something" -> "Content")
    sGraph.addMessage(1,  "author1",  "content1",  someMap)
    sGraph.addMessage(2,  "author2",  "content2",  someMap)
    sGraph.addMessage(3,  "author3",  "content3",  someMap)
    sGraph.addMessage(4,  "author4",  "content4",  someMap)
    sGraph.addMessage(5,  "author5",  "content5",  someMap)
    sGraph.addMessage(6,  "author6",  "content6",  someMap)
    sGraph.addMessage(7,  "author7",  "content7",  someMap)
    sGraph.addMessage(8,  "author8",  "content8",  someMap)
    sGraph.addMessage(9,  "author9",  "content9",  someMap)
    sGraph.addMessage(10, "author10", "content10", someMap)

    sGraph.addConnection(2,  1, "noone")
    sGraph.addConnection(3,  1, "noone")
    sGraph.addConnection(4,  2, "noone")
    sGraph.addConnection(5,  2, "noone")
    sGraph.addConnection(6,  2, "noone")
    sGraph.addConnection(7,  5, "noone")
    sGraph.addConnection(8,  5, "noone")
    sGraph.addConnection(9,  3, "noone")
    sGraph.addConnection(10, 3, "noone")

    val calcs: RelationshipsCalcs = new RelationshipsCalcs()
    calcs.calcDefeaters(sGraph)
  }

  def optimizedDefeatersCountTest(){
    //Create a graph with min depth 2 and max depth 3
    val sGraph : SocialGraph = new SocialGraph(context)
    val someMap : Map[String, Any] = Map("Something" -> "Content")
    sGraph.addMessage(1,  "author1",  "content1",  someMap)
    sGraph.addMessage(2,  "author2",  "content2",  someMap)
    sGraph.addMessage(3,  "author3",  "content3",  someMap)
    sGraph.addMessage(4,  "author4",  "content4",  someMap)
    sGraph.addMessage(5,  "author5",  "content5",  someMap)
    sGraph.addMessage(6,  "author6",  "content6",  someMap)
    sGraph.addMessage(7,  "author7",  "content7",  someMap)
    sGraph.addMessage(8,  "author8",  "content8",  someMap)
    sGraph.addMessage(9,  "author9",  "content9",  someMap)
    sGraph.addMessage(10, "author10", "content10", someMap)

    sGraph.addConnection(2,  1, "noone")
    sGraph.addConnection(3,  1, "noone")
    sGraph.addConnection(4,  2, "noone")
    sGraph.addConnection(5,  2, "noone")
    sGraph.addConnection(6,  2, "noone")
    sGraph.addConnection(7,  5, "noone")
    sGraph.addConnection(8,  5, "noone")
    sGraph.addConnection(9,  3, "noone")
    sGraph.addConnection(10, 3, "noone")

    val calcs: RelationshipsCalcs = new RelationshipsCalcs()
    calcs.optimizedCalcDefeaters(sGraph)
  }

  def downloadConversation(){
    val testGraph : TwitterGraph = getMultiReferenceConversationGraph()
  }

  def downloadRedditConversation(){
    val downloader = new RedditDownloader(context)
    val testGraph : RedditGraph = downloader.getGraphFromConversationId("63w4sq","Downloaders/reddit-collector/reddit.json")
  }

  private def getMultiReferenceConversationGraph(): TwitterGraph = {
    storedTestGraph match {
      case Some(i) => i
      case None =>  {
        val downloader = new TwitterDownloader(session.sparkContext)
        val path = downloader.downloadAndStoreConversation("574324656905281538")
    
        //Create MPC
        val parserCaller = new ParserCaller(session)
        val classifier = parserCaller.parseJson(path)
        //Create graph
        val testGraph = new TwitterGraph(session.sparkContext, classifier)
        testGraph.parseConversationFromFile(path)
        storedTestGraph = Some(testGraph)
        testGraph
      }
    }
  }

  var storedTestGraph : Option[TwitterGraph] = None

}
