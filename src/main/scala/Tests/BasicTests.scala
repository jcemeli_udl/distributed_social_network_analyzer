//SparkContext & Session
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX TODO: TEST ONLY
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

//Read files
import scala.io.Source

class BasicTests(session: SparkSession){

  val context = session.sparkContext

  def doAllTests(){
    graphParsingTest()
    graphCreatingTest()
    parseSingleTweetTest()
    parseTweetConversation()
    multireferenceTweetConversation()
  }

  def graphParsingTest(){
    val jGraph = new SocialGraph(context)

    //Make the connections, both systems are equivalent

    jGraph.parseConnection("{\"origin\":1," +
                            "\"destination\":2," +
                            "\"intention\":\"noone\"}")
    jGraph.parseConnection("{\"origin\":1," +
                            "\"destination\":3," +
                            "\"intention\":\"noone\"}")

    //Make the messages identifier, author and Json with properties)

    jGraph.parseMessage("{\"id\":1," +
                         "\"weight\":1," +
                         "\"author\":\"author1\"," +
                         "\"message\":\"content1\"," +
                         "\"properties\":{\"property\":\"something\"}}")
    jGraph.parseMessage("{\"id\":2," +
                         "\"weight\":1," +
                         "\"author\":\"author2\"," +
                         "\"message\":\"content1\"," +
                         "\"properties\":{\"property\":\"something\"}}")
    jGraph.parseMessage("{\"id\":3," +
                         "\"weight\":1," +
                         "\"author\":\"author3\"," +
                         "\"message\":\"content2\"," +
                         "\"properties\":{\"property\":\"something\"}}")

    jGraph.makeGraph()

    val graph = jGraph.getGraph()

    val contentCount = graph.vertices.filter{
      case(id, message) => message.message == "content1"
    }.count

    val relationToRootCount = graph.edges.filter(e => e.srcId < e.dstId).count


    println("Total messages with content 1: " + contentCount + "\n" +
      "Total edges going to higher node id: " + relationToRootCount)
  }

  def listSubGraphTriplets(){
    val jGraph = new SocialGraph(context)
    jGraph.addConnection(2, 1, "attack")
    jGraph.addConnection(3, 2, "support")
    jGraph.addConnection(4, 1, "support")
    jGraph.addConnection(5, 4, "attack")
    jGraph.addConnection(6, 4, "noone")
    val someMap : Map[String, Any] = Map("Something" -> "Content")
    jGraph.addMessage(1, "author1", "content1", someMap, 1)
    jGraph.addMessage(2, "author2", "content2", someMap, 2)
    jGraph.addMessage(3, "author3", "content3", someMap, 3)
    jGraph.addMessage(4, "author4", "content4", someMap, 1)
    jGraph.addMessage(5, "author5", "content5", someMap, 2)
    jGraph.addMessage(6, "author6", "content6", someMap, 3)

    val graph = jGraph.getSubGraph(true, false, false).getGraph()
    val facts: RDD[String] =
    graph.triplets.map(triplet =>
    triplet.srcAttr.author + " is " + triplet.attr + " with " + triplet.dstAttr.author)
    facts.collect.foreach(println(_))
  }

  def graphCreatingTest(){
    val jGraph = new SocialGraph(context)
    jGraph.addConnection(1, 2, "noone")
    jGraph.addConnection(1, 3, "noone")
    val someMap : Map[String, Any] = Map("Something" -> "Content")
    jGraph.addMessage(1, "author1", "content1", someMap, 1)
    jGraph.addMessage(2, "author2", "content2", someMap, 2)
    jGraph.addMessage(3, "author3", "content3", someMap, 3)

    jGraph.makeGraph()

    val graph = jGraph.getGraph()

    val contentCount = graph.vertices.filter{
      case(id, message) => message.message == "content1"
    }.count

    val relationToRootCount = graph.edges.filter(e => e.srcId < e.dstId).count


    println("Total messages with content 1: " + contentCount + "\n" +
      "Total edges going to higher node id: " + relationToRootCount)
  }

  def parseSingleTweetTest(){

    //Create MPC
    val path = "Examples/JsonSingleTweet.txt"
    val parserCaller = new ParserCaller(session)
    val classifier = parserCaller.parseJson(path)
    //Create graph
    val tGraph = new TwitterGraph(context, classifier)
    tGraph.parseConversationFromFile(path)
    val line = Source.fromFile(path).getLines().next()
    println("Let's parse the next tweet: \n" + line)
    tGraph.parseTweet(line)
  }

  def parseTweetConversation(){
    //Create MPC
    val path = "Examples/JsonConversationTweets.txt"
    val parserCaller = new ParserCaller(session)
    val classifier = parserCaller.parseJson(path)
    //Create graph
    val tGraph = new TwitterGraph(context, classifier)
    tGraph.parseConversationFromFile(path)
  }

  def multireferenceTweetConversation(){
    println("[TEST]: Let's parse MultiReferenceConversation")

    //Create MPC
    val path = "Examples/JsonMultireferenceConversation.txt"
    val parserCaller = new ParserCaller(session)
    val classifier = parserCaller.parseJson(path)
    //Create graph
    val tGraph = new TwitterGraph(context, classifier)
    tGraph.parseConversationFromFile(path)
  }

  def parseRedditConversation(){
    println("[TEST]: Let's parse Reddit conversation from file")
    val rGraph = new RedditGraph(context)
    val filePath = "Examples/FakeRedditConversation.txt"
    rGraph.parseRedditConversationFromFile(filePath)
  }

  def updatePunctuation(){

    val jGraph = new SocialGraph(context)

    //Make the connections, both systems are equivalent

    jGraph.parseConnection("{\"origin\":1," +
                            "\"destination\":2," +
                            "\"intention\":\"noone\"}")
    jGraph.parseConnection("{\"origin\":1," +
                            "\"destination\":3," +
                            "\"intention\":\"noone\"}")

    //Make the messages identifier, author and Json with properties)

    jGraph.parseMessage("{\"id\":1," +
                         "\"weight\":1," +
                         "\"author\":\"author1\"," +
                         "\"message\":\"content1\"," +
                         "\"properties\":{\"property\":\"something\"}}")
    jGraph.parseMessage("{\"id\":2," +
                         "\"weight\":1," +
                         "\"author\":\"author2\"," +
                         "\"message\":\"content1\"," +
                         "\"properties\":{\"property\":\"something\"}}")
    jGraph.parseMessage("{\"id\":3," +
                         "\"weight\":1," +
                         "\"author\":\"author3\"," +
                         "\"message\":\"content2\"," +
                         "\"properties\":{\"property\":\"something\"}}")

    jGraph.makeGraph()

    val graph = jGraph.getGraph()

    val contentCount = graph.vertices.filter{
      case(id, message) => message.message == "content1"
    }.count

    val relationToRootCount = graph.edges.filter(e => e.srcId < e.dstId).count


    println("Total messages with content 1: " + contentCount + "\n" +
      "Total edges going to higher node id: " + relationToRootCount)

    val facts: RDD[String] =
    graph.triplets.map(triplet =>
    triplet.srcAttr.author + " with " + triplet.srcAttr.weight + " is " + triplet.attr + " with " + triplet.dstAttr.author + " with " + triplet.dstAttr.weight)

    //Update values
    jGraph.updatePunctuation(1, 4)
    jGraph.makeGraph()
    val newFacts: RDD[String] =
    jGraph.getGraph().triplets.map(triplet =>
    triplet.srcAttr.author + " with " + triplet.srcAttr.weight + " is " + triplet.attr + " with " + triplet.dstAttr.author + " with " + triplet.dstAttr.weight)

    println("Old values...")
    facts.collect.foreach(println(_))
    println("New values...")
    newFacts.collect.foreach(println(_))

  }

}
