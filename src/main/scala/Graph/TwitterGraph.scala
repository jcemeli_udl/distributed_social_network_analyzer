//SparkContext
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//Json4s
import org.json4s._
import org.json4s.jackson.JsonMethods._

//GraphX
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

/***Summary***
TwitterGraph is the specific parser for twitter, it takes a Twitter
conversation or a specific tweets jsons (obtained directly from twitter API)
and SetUp a SocialGraph with this information.
*************/

/***Conversation Json***
There are two main ways to parse a conversation:

1st: Do it tweet by tweet: place on every line a json from a tweet from the
conversation and read it line by line and parse them using parseTweet.

2nd: Using a conversation json that must be composed on that way:
{"tweets":[{jsonTweet1}, {jsonTweet2}, {jsonTweet3}]}
Where a jsonTweet is a json of a tweet obtained directly from twitter API
***********************/

class TwitterGraph(context: SparkContext, classifier: MPC) extends SocialGraph(context){
  var userReferences : Map[String, List[Long]] = Map()
  var focusTo : Map[Long, Option[Long]] = Map().withDefaultValue(None)
  var tweetAuthor : Map[Long, String] = Map()

  def parseConversationFromFile(filePath: String){
    val file = context.textFile(filePath).toLocalIterator.toArray
    for (line <- file){
      parseTweet(line)
    }
  }

  def parseConversation(jsonConversation: String){
    val conversation = parse(jsonConversation).extract[Twitter_Conversation_Tweets]
    val conversation_properties = parse(jsonConversation).extract[Twitter_Conversation_Properties]
    for(i <- 0 until conversation.tweets.size){
      val l_tweet = conversation.tweets(i)
      val l_properties = conversation_properties.tweets(i)
      println("Tweet readed:" +
              "\n\tId:"       + l_tweet.id      +
              "\n\tAuthor: "  + l_tweet.user("screen_name").toString  +
              "\n\tMessage: " + l_tweet.text)
      addMessage(l_tweet.id, l_tweet.user("screen_name").toString,
      l_tweet.text, l_properties, weightCalc(l_tweet.user("followers_count").asInstanceOf[Number].intValue,
      l_tweet.retweet_count.toInt, l_tweet.favorite_count.toInt))
      tweetAuthor += (l_tweet.id -> l_tweet.user("screen_name").toString)
      println("Author of this tweet is: " + tweetAuthor(l_tweet.id))
      makeLinks(l_tweet.id, l_properties, l_tweet.entities.user_mentions)
    }
  }

  private def weightCalc(followers : Int, retweets : Int, favorites : Int) : Float = {
    var sum : Double = followers
    sum += 20 * retweets
    sum += 40 * favorites
    sum += 1
    (scala.math.log(sum)/scala.math.log(2)).toFloat
  }

  def parseTweet(jsonTweet: String){
    parseConversation("{\"tweets\":["+jsonTweet+"]}")
  }

  private def makeLinks(id: Long, info: Map[String, Any],
    mentions: List[Map[String,Any]]){
    //Make basic link
    val reply_to = info("in_reply_to_status_id")
    if(reply_to != null){
      val l_reply_to:Long = reply_to.asInstanceOf[Number].longValue
      focusTo += (id -> Some(l_reply_to))
      println("Link info:"+
              "\n\tOrigin: " + id +
              "\n\tDestin: " + l_reply_to)
      addConnection(id, l_reply_to, classifier.getRelationship(id,l_reply_to))
      val mentionList : ListBuffer[String] = getMentionList(mentions)
      println("Mention list: ")
      for(mention <- mentionList){
        println("\t-" + mention)
      }
      makeFullLinks(id, l_reply_to, mentionList)
    }
    else{
      println("This is the root tweet")
    }
  }

  private def makeFullLinks(id: Long, reply_to: Long,
    mentions: ListBuffer[String]){
    var nextLookFocus = reply_to
    var focus: Option[Long] = None
    for(mention <- mentions){
      do {
       focus = focusTo.getOrElse(nextLookFocus, None)
       focus match{
         case Some(s) => {
           val currAuthor = tweetAuthor.getOrElse(s, None)
           if(currAuthor != None && currAuthor == mention && reply_to != s){
             println(println("Link info:"+
                     "\n\tOrigin: " + id +
                     "\n\tDestin: " + s))
           }
           addConnection(id, s, classifier.getRelationship(id, s))
           nextLookFocus = s
         }
         case None => {
           nextLookFocus = reply_to
         }
       }
      }
      while(focus != None)
    }
  }

  private def getMentionList(mentions: List[Map[String, Any]]) : ListBuffer[String]= {
    val mentionList: ListBuffer[String] = ListBuffer[String]()
    for(mention <- mentions){
      val name : String = mention("screen_name").toString
      if(!mentionList.contains(name))
        mentionList += name
    }
    mentionList.distinct
    mentionList
  }

}

case class Tweet_Temp(id: Long, user: Map[String, Any], text: String,
  entities : Entities_Temp, retweet_count : Long, favorite_count : Long)
case class Entities_Temp(user_mentions: List[Map[String, Any]])
case class Twitter_Conversation_Tweets(tweets: List[Tweet_Temp])
case class Twitter_Conversation_Properties(tweets: List[Map[String, Any]])
