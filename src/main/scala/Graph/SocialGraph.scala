//SparkContext
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Json4s
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization

//Lists
import scala.collection.mutable.ListBuffer

//Serialization
import java.io._

/***Summary***
JsonGraph it's and abstraction for Graph creation, allowing creating messages
and connections that could be added directly or parsed from a Json

Every message must contain an identifier an author, a message and a Json filled
with properties.

Every connection will represent the edge between two messages, represented by
source and destination identifiers and the kind of relation between them.
************/

class SocialGraph(context: SparkContext){
    var m_context = context
    var m_messages : Map[Long, Message] = Map() //ID -> Message
    var m_connections : Map[(Long, Long), Edge[String]] = Map() //(From, To) -> Edge
    var g_graph : Option[Graph[Message, String]] = None
    val m_representations = new GraphRepresentations(this, context)
    implicit val formats = DefaultFormats
    
    def getSubGraph(attack: Boolean , support: Boolean, noone: Boolean) : SocialGraph = {
      var resultGraph : SocialGraph = new SocialGraph(context)
      var subGraphMessages : Map[Long, Message] = Map()
      //Look for desired connections
      for (connection <- m_connections.values.toList){
        if((attack && connection.attr == "attack") ||
          (support && connection.attr == "support") ||
          (noone && connection.attr == "noone")){
            if(!subGraphMessages.contains(connection.srcId)){
              val srcMessage : Message = m_messages(connection.srcId)
              subGraphMessages += (connection.srcId -> srcMessage)
              resultGraph.addMessage(srcMessage.id, srcMessage.author, srcMessage.message,
              srcMessage.properties, srcMessage.weight)
            }
            if(!subGraphMessages.contains(connection.dstId)){
              val dstMessage : Message = m_messages(connection.dstId)
              subGraphMessages += (connection.srcId -> dstMessage)
              resultGraph.addMessage(dstMessage.id, dstMessage.author, dstMessage.message,
              dstMessage.properties, dstMessage.weight)
            }
            resultGraph.addConnection(connection.srcId, connection.dstId, connection.attr)
        }
      }
      resultGraph
    }

    def updatePunctuation(id : Long, newPunctuation : Float){
      m_messages(id).weight = newPunctuation
    }

    def storeGraph(directory : String, graphName : String){
      //Create files
      val file_messages = new File(directory + graphName + ".mes")
      val file_connections = new File(directory + graphName + ".con")
      val writer_messages = new BufferedWriter(new FileWriter(file_messages))
      val writer_connections = new BufferedWriter(new FileWriter(file_connections))
      var exp_json : String = ""
      var map_json : String = ""

      for (message <- m_messages.values.toList){

          exp_json += "{\"id\":"        + message.id              + ","   +
                      "\"weight\":"     + message.weight          + ","   +
                      "\"author\":\""   + message.author          + "\"," +
                      "\"message\":\""  + message.message.replace("\"", "\\\"").replace("\n", " ") + "\"," +
                      "\"properties\":" + Serialization.write(message.properties)  + "}"
          writer_messages.write(exp_json)
          exp_json = "\n"
      }

      exp_json = ""
      for (connection <- m_connections.values.toList){
        exp_json += "{\"origin\":"     + connection.srcId     + "," +
                   "\"destination\":" + connection.dstId      + "," +
                   "\"intention\":\"" + connection.attr       + "\"}"
        writer_connections.write(exp_json)
        exp_json = "\n"
      }

      //Close files
      writer_messages.close()
      writer_connections.close()
    }

    def loadGraph(directory : String, graphName : String){
      //Open files
      val baseFilename = directory + graphName
      val file_messages = scala.io.Source.fromFile(baseFilename + ".mes")
      val file_connections = scala.io.Source.fromFile(baseFilename + ".con")

      //Load messages
      var lines = file_messages.getLines()
      var message : String = ""
      var count : Int = 0

      for (line <- lines){
          parseMessage(line)
          count += 1
      }

      //Load connections
      lines = file_connections.getLines()
      for (line <- lines){
        parseConnection(line)
      }

      //Close files
      file_messages.close()
      file_connections.close()

    }

    def makeGraph(){
      //Create vertex
      val g_vertex : RDD[(VertexId, Message)] = m_context.parallelize(messagesVertexFormat())
      //Create edges
      val g_edges : RDD[Edge[String]] = m_context.parallelize(m_connections.values.to[collection.immutable.Seq])
      //Create default if no relationship
      val defaultMap : Map[String, Any] = Map("Invalid" -> "Invalid")
      val defaultRelation = new Message(-1L, 0, "Invalid", "Invalid", defaultMap)

      //Build the graph
      g_graph = Some(Graph(g_vertex, g_edges, defaultRelation))
    }

    def getGraph(): Graph[Message, String] = {
      g_graph match{
        case None => {
          makeGraph()
          getGraph()
        }
        case Some(s) => s
      }
    }

    def addMessage(id: Long, author: String, message: String,
       properties: Map[String, Any], weight : Float = -1){
      m_messages += (id -> new Message(id, weight, author, message, properties))
    }

    def addConnection(origin: Long, destination: Long, intention: String){
      m_connections += ((origin, destination) -> new Edge(origin, destination, intention))
    }

    /***Summary***
    Calculates the max of the distances of the graph.
    *************/
    def maxDistance() : Int = {
      val graph = m_representations.integerOnly(0)
      val sssp = graph.pregel(0)(
        (id, dist, newDist) => {
          newDist
        },
        triplet => {
          if(triplet.dstAttr <= triplet.srcAttr){
            Iterator((triplet.dstId, triplet.srcAttr+1))
          }
          else{
            Iterator.empty
          }
        },
        (a,b) => {
          if(a>b) a
          else b
        }
      )
      val distance = sssp.vertices.reduce((a,b) => {
        if(a._2 > b._2) a
        else b
      })
      println(sssp.vertices.collect.mkString("\n"))
      distance._2
    }

    /***Summary***
    Calculates the min of the distances of the graph.
    *************/
    def minDistance() : Int = {
      val graph = m_representations.integerBool(0, true)
      val sssp = graph.pregel((0, true))(
        (id, dist, newDist) => {
          if(dist._1 == newDist._1 && dist._1 != 0){
            (dist._1, false)
          }
          else{
            newDist
          }
        },
        triplet => {
          if(triplet.dstAttr._2){
            Iterator((triplet.dstId, (triplet.srcAttr._1+1, true)))
          }
          else{
            Iterator.empty
          }
        },
        (a,b) => {
          if(a._1 < b._1) a
          else b
        }
      )
      val distance = sssp.vertices.reduce((a,b) => {
        if(a._2._1 > b._2._1) a
        else b
      })
      println(sssp.vertices.collect.mkString("\n"))
      distance._2._1
    }

    /***Summary***
    Calculates the average of the distances of the graph.
    *************/
    def averageDistance() : Float = {

      //First acquire the summ of all the distances
      val graph = m_representations.integerIntegerBool()
      val sssp = graph.pregel((-1, -1, true))(
        (id, dist, newDist) => {
          if(newDist._1 == -1){
            (0,0,true)
          }
          else{
            if(dist._1 == newDist._1 && dist._2 == newDist._2){
              (dist._1, dist._2, false)
            }
            else{
              newDist
            }
          }
        },
        triplet => {
          if(triplet.dstAttr._3){
            if(triplet.srcAttr._1 == 0){
              Iterator((triplet.dstId, (1, 1, true)))
            }
            else{
              Iterator((triplet.dstId, (triplet.srcAttr._1+triplet.srcAttr._2, triplet.srcAttr._2, true)))
            }
          }
          else{
            Iterator.empty
          }
        },
        (a,b) => {
          (a._1+b._1, a._2+b._2, true)
        }
      )
      val distance = sssp.vertices.reduce((a,b) => {
        if(a._2._1 > b._2._1) a
        else b
      })
      //println(sssp.vertices.collect.mkString("\n"))
      //Divide total distances between all the leafs.
      val leafs = (graph.vertices.count - graph.inDegrees.count)
      //println("Total leafs: " + leafs)
      //println("Total distance " + distance._2._1)
      val avg : Float = (distance._2._1.toFloat/leafs.toFloat)
      //Return average
      avg
    }

    def getMessages(): Map[Long, Message] = m_messages

    def getConnections(): Map[(Long, Long), Edge[String]] = m_connections

    def getRepresentation(): GraphRepresentations = m_representations

    def parseMessage(messageJson : String){
      val parsedMessage : Message = parse(messageJson).extract[Message]
      m_messages += (parsedMessage.id -> parsedMessage)
    }

    def parseConnection(connectionJson : String){
      val conn = parse(connectionJson).extract[Connection]
      m_connections += ((conn.origin, conn.destination) -> new Edge(conn.origin, conn.destination, conn.intention))
    }

    private def messagesVertexFormat() : Array[(VertexId, Message)] = {
      val returnArray = new Array[(VertexId, Message)](m_messages.toList.length)
      val messagesArray = m_messages.toList.toArray
      for(i <- 0 until messagesArray.length){
        returnArray(i) = (messagesArray(i)._1, messagesArray(i)._2)
      }
      returnArray
    }

}

case class Message(id: Long, var weight: Float, author: String, message: String, properties: Map[String, Any])
case class Connection (origin: Long, destination: Long, intention: String)
