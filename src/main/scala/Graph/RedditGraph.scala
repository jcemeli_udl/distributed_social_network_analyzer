//SparkContext
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//Json4s
import org.json4s._
import org.json4s.jackson.JsonMethods._

//GraphX
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

/***Summary***
RedditGraph is the specific parser for reddit, it takes a Reddit
conversation or a specific reddit jsons (obtained directly from reddit API)
and SetUp a SocialGraph with this information.
*************/

class RedditGraph(context: SparkContext) extends SocialGraph(context){

  def parseRedditConversationFromFile(filePath: String){
    val file = scala.io.Source.fromFile(filePath)
    val lines = file.getLines()
    for (line <- lines){
      parseRedditMessage(line)
    }
    file.close()
  }

  def parseRedditConversation(jsonConversation: String){
    val conversation = parse(jsonConversation).extract[Reddit_Conversation_Messages]
    val conversation_properties = parse(jsonConversation).extract[Reddit_Conversation_Properties]
    for(i <- 0 until conversation.messages.size){
      val l_message = conversation.messages(i)
      val l_properties = conversation_properties.messages(i)
      println("Message readed:"+
                            "\n\tId:"       + l_message.id      +
                            "\n\tAuthor: "  + l_message.author_name  +
                            "\n\tMessage: " + l_message.text)
      addMessage(l_message.id, l_message.author_name, l_message.text, l_properties)
      if(l_message.parent != -1){
        addConnection(l_message.id, l_message.parent, "noone") //TODO: Add real intention
      }
    }
  }

  def parseRedditMessage(jsonMessage: String){
    parseRedditConversation("{\"messages\":["+jsonMessage+"]}")
  }

}

case class Reddit_Temp(id: Long, author_name: String, text: String, parent : Long)
case class Reddit_Conversation_Messages(messages: List[Reddit_Temp])
case class Reddit_Conversation_Properties(messages: List[Map[String, Any]])
