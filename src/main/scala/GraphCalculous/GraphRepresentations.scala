//SparkContext
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

class GraphRepresentations(val originalGraph: SocialGraph,
                           val context: SparkContext){

    /***Summary***
    This class provides different graph representations from SocialGraph
    ************/

    /**Summary**
    Gives a Integer graph representation
    with the same struct and connections of the original.
    ***********/
    def integerOnly(initValue : Integer): Graph[Int, String] = {
      //Replicate vertices
      var replicatedVertices : ListBuffer[(VertexId, Int)] =
        new ListBuffer[(VertexId, Int)]
      originalGraph.getMessages().map(a => replicatedVertices += ((a._1, initValue)))
      val g_vertex : RDD[(VertexId, Int)] = context.parallelize(replicatedVertices)
      //Replicate connections
      val g_edges : RDD[Edge[String]] = context.parallelize(originalGraph.getConnections().values.to[collection.immutable.Seq])
      //Build the graph
      Graph(g_vertex, g_edges)
    }

    /**Summary**
    Gives a Integer,Integer graph representation
    with the same struct and connections of the original.
    ***********/
    def integerInteger(): Graph[(Int, Int), String] = {
      //Replicate vertices
      var replicatedVertices : ListBuffer[(VertexId, (Int, Int))] =
        new ListBuffer[(VertexId, (Int, Int))]
      originalGraph.getMessages().map(a => replicatedVertices += ((a._1, (0, 0))))
      val g_vertex : RDD[(VertexId, (Int, Int))] = context.parallelize(replicatedVertices)
      //Replicate connections
      val g_edges : RDD[Edge[String]] = context.parallelize(originalGraph.getConnections().values.to[collection.immutable.Seq])
      //Create default if no relationship
      //val defaultRelation = 0
      //Build the graph
      Graph(g_vertex, g_edges)//, defaultRelation)
    }

    /**Summary**
    Gives a Integer,Bool graph representation
    with the same struct and connections of the original
    in order to i.e. stop sending messages to a node marked as 'false'
    in pregel.
    ***********/
    def integerBool(intVal:Integer, boolVal:Boolean): Graph[(Int, Boolean), String] = {
      //Replicate vertices
      var replicatedVertices : ListBuffer[(VertexId, (Int, Boolean))] =
        new ListBuffer[(VertexId, (Int, Boolean))]
        originalGraph.getMessages().map(a => replicatedVertices += ((a._1, (intVal, boolVal))))
        val g_vertex : RDD[(VertexId, (Int, Boolean))] = context.parallelize(replicatedVertices)
        //Replicate connections
        val g_edges : RDD[Edge[String]] = context.parallelize(originalGraph.getConnections().values.to[collection.immutable.Seq])
        //Create default if no relationship
        //val defaultRelation = 0
        //Build the graph
        Graph(g_vertex, g_edges)//, defaultRelation)
    }

    /**Summary**
    Gives a Integer,Integer,Boolean graph representation
    with the same struct and connections of the original.
    ***********/
    def integerIntegerBool(): Graph[(Int, Int, Boolean), String] = {
      //Replicate vertices
      var replicatedVertices : ListBuffer[(VertexId, (Int, Int, Boolean))] =
        new ListBuffer[(VertexId, (Int, Int, Boolean))]
      originalGraph.getMessages().map(a => replicatedVertices += ((a._1, (0, 0, true))))
      val g_vertex : RDD[(VertexId, (Int, Int, Boolean))] = context.parallelize(replicatedVertices)
      //Replicate connections
      val g_edges : RDD[Edge[String]] = context.parallelize(originalGraph.getConnections().values.to[collection.immutable.Seq])
      //Create default if no relationship
      //val defaultRelation = 0
      //Build the graph
      Graph(g_vertex, g_edges)//, defaultRelation)
    }

    /**Summary**
    Gives a Float,Integer,Boolean graph representation
    with the same struct and connections of the original.
    The first value can be setted with the weight of the node.
    ***********/
    def FloatIntegerBool(first: Float = 0, second: Integer = 0,
      third: Boolean = true, firstIsWeight: Boolean = false,
      leafsBool: Boolean = true):
      Graph[(Float, Int, Boolean), String] = {
      //Replicate vertices
      var replicatedVertices : ListBuffer[(VertexId, (Float, Int, Boolean))] =
        new ListBuffer[(VertexId, (Float, Int, Boolean))]
      if(firstIsWeight){
        originalGraph.getMessages().map(a => {
          var initValue : Boolean = leafsBool
          if(originalGraph.getGraph().inDegrees.lookup(a._1) != Seq.empty[Int]){
            initValue = third
          }
          replicatedVertices += ((a._1, (a._2.weight, second, initValue)))
        })
      }
      else{
        originalGraph.getMessages().map(a => {
          var initValue : Boolean = leafsBool
          if(originalGraph.getGraph().inDegrees.lookup(a._1) != Seq.empty[Int]){
            initValue = third
          }
          replicatedVertices += ((a._1, (first, second, initValue)))
        })
      }
      val g_vertex : RDD[(VertexId, (Float, Int, Boolean))] = context.parallelize(replicatedVertices)
      //Replicate connections
      val g_edges : RDD[Edge[String]] = context.parallelize(originalGraph.getConnections().values.to[collection.immutable.Seq])
      //Create default if no relationship
      //val defaultRelation = 0
      //Build the graph
      Graph(g_vertex, g_edges)//, defaultRelation)
    }

      def FloatIntegerLongBool(first: Float = 0, second: Integer = 0, third: Long = 0,
      fourth: Boolean = true, firstIsWeight: Boolean = false,
      leafsBool: Boolean = true):
      Graph[(Float, Int, Long, Boolean), String] = {
      //Replicate vertices
      var replicatedVertices : ListBuffer[(VertexId, (Float, Int, Long, Boolean))] =
        new ListBuffer[(VertexId, (Float, Int, Long, Boolean))]
      if(firstIsWeight){
        originalGraph.getMessages().map(a => {
          var initValue : Boolean = leafsBool
          if(originalGraph.getGraph().inDegrees.lookup(a._1) != Seq.empty[Int]){
            initValue = fourth
          }
          replicatedVertices += ((a._1, (a._2.weight, second, third, initValue)))
        })
      }
      else{
        originalGraph.getMessages().map(a => {
          var initValue : Boolean = leafsBool
          if(originalGraph.getGraph().inDegrees.lookup(a._1) != Seq.empty[Int]){
            initValue = fourth
          }
          replicatedVertices += ((a._1, (first, second, third, initValue)))
        })
      }
      val g_vertex : RDD[(VertexId, (Float, Int, Long, Boolean))] = context.parallelize(replicatedVertices)
      //Replicate connections
      val g_edges : RDD[Edge[String]] = context.parallelize(originalGraph.getConnections().values.to[collection.immutable.Seq])
      //Create default if no relationship
      //val defaultRelation = 0
      //Build the graph
      Graph(g_vertex, g_edges)//, defaultRelation)
    }


}
