//SparkContext
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

class RelationshipsCalcs(){

    /*****Summary*****
    |Graph represents Weight / Defeaters / Finished
    |Defeaters: -1 at initial state, to set it's value at least one time,
    |avoiding mark them as finished
    |Finished: True / False
    *****************/
    def calcDefeaters(sGraph: SocialGraph) : Int = {
      // Init graph to (Weight, -1, False)
      val graph = sGraph.getSubGraph(true, false, false).getRepresentation().FloatIntegerBool(0, -1, false, true, true)

      val sssp = graph.pregel((0, 0, false))(
        (id, main, received) => {
          //Evaluate the joined messages and set the new state
          if((main._2 == received._2 && received._3) || main._3){
            //If the state it's the same as previous mark as finished
            (main._1, received._2, true)
          }
          else{
            (main._1, received._2, false)
          }
        },
        triplet => {
          //Compute the messages to send
          if(triplet.srcAttr._1 >= triplet.dstAttr._1 && //Weight
             triplet.dstAttr._3 == false){               //Dst is not finished
            //Send a message
            if(triplet.srcAttr._2 == 0){
              Iterator((triplet.dstId, (0, 1, triplet.srcAttr._3)))
            }
            else{
              Iterator((triplet.dstId, (0, 0, triplet.srcAttr._3)))
            }
          }
          else{
            //Don't send a message
            Iterator.empty
          }
        },
        (a,b) => {
          //Join messages into one
          (0, a._2 + b._2, a._3 && b._3)
        }
      )
      val defeaterNodes = sssp.vertices.map(node => {
        if(node._2._2 > 0){
          0
        }
        else{
          1
        }
      }).reduce((a, b) => a+b)
      println("TOTAL DEFEATERS: " + defeaterNodes)
      defeaterNodes

    }


    def optimizedCalcDefeaters(sGraph: SocialGraph) : Int = {
      // Init graph to (Weight, -1, False)
      val graph = sGraph.getSubGraph(true, false, false).getRepresentation().FloatIntegerBool(0, -1, false, true, true)

      val sssp = graph.pregel((0, 0, false))(
        (id, main, received) => {
          //Evaluate the joined messages and set the new state
          if((main._2 == received._2) || main._3){
            //If the state it's the same as previous mark as finished
            (main._1, received._2, true)
          }
          else{
            if(!received._3){
              (main._1, received._2, false)
            }
            else{
              (main._1, received._2, true)
            }
          }
        },
        triplet => {
          //Compute the messages to send
          if(triplet.srcAttr._1 >= triplet.dstAttr._1 && //Weight
             triplet.srcAttr._3 && !triplet.dstAttr._3){               //Dst is not finished
            //Send a message
            if(triplet.srcAttr._2 == 0){
              Iterator((triplet.dstId, (0, 1, triplet.srcAttr._3)))
            }
            else{
              Iterator((triplet.dstId, (0, 0, triplet.srcAttr._3)))
            }
          }
          else{
            //Don't send a message
            Iterator.empty
          }
        },
        (a,b) => {
          //Join messages into one
          (0, a._2 + b._2, true)
        }
      )
      val defeaterNodes = sssp.vertices.map(node => {
        if(node._2._2 > 0){
          0
        }
        else{
          1
        }
      }).reduce((a, b) => a+b)
      println("TOTAL DEFEATERS: " + defeaterNodes)
      defeaterNodes
    }



        def calcDefeatersMessageCount(sGraph: SocialGraph) : Long = {
      // Init graph to (Weight, -1, False)
      println("Preparing environment for calculate message count")
      val graph = sGraph.getSubGraph(true, false, false).getRepresentation().FloatIntegerLongBool(0, 0, 0, false, true, true)
      //println("USED VERTICES: " + graph.numVertices)
      val sssp = graph.pregel((0, -1, 0, false))(
        (id, main, received) => {
          //Initial message, avoid mark leafs as unfinished
          if(received._2 == -1){
            (main._1, 0, 0, main._4)
          }
          else{
            //If the state it's the same as previous mark as finished
            (main._1, received._2, main._3 + received._3, main._2 == received._2 || received._4)
          }  
        },
        triplet => {
          //Vote to finish
          if(triplet.srcAttr._4 && triplet.dstAttr._4){
            Iterator.empty
          }
          else{
            //Compute the messages to send
            //Send a message
            if(triplet.srcAttr._2 == 0 && triplet.srcAttr._1 >= triplet.dstAttr._1){
              Iterator((triplet.dstId, (0, 1, 1, triplet.srcAttr._4)))
            }
            else{
              Iterator((triplet.dstId, (0, 0, 1, triplet.srcAttr._4)))
            }
          }
        },
        (a,b) => {
          //Join messages into one
          (0, a._2 + b._2, a._3 + b._3, a._4 && b._4)
        }
      )
      val totalMessages = sssp.vertices.reduce((a,b) => (0, (0, 0, a._2._3 + b._2._3, true)))
      println("TOTAL MESSAGES: " + totalMessages._2._3)
      val defeaterNodes = sssp.vertices.map(node => {
        if(node._2._2 > 0){
          0
        }
        else{
          1
        }
      }).reduce((a, b) => a+b)
      println("TOTAL DEFEATERS: " + defeaterNodes)
      defeaterNodes
    }


    def optimizedCalcDefeatersMessageCount(sGraph: SocialGraph) : Long = {
      // Init graph to (Weight, -1, False)
      val graph = sGraph.getSubGraph(true, false, false).getRepresentation().FloatIntegerLongBool(0, -1, 0, false, true, true)

      val sssp = graph.pregel((0, 0, 0, false))(
        (id, main, received) => {
          //Evaluate the joined messages and set the new state
          if((main._2 == received._2) || main._4){
            //If the state it's the same as previous mark as finished
            (main._1, received._2, main._3 + received._3, true)
          }
          else{
            if(!received._4){
              (main._1, received._2, main._3 + received._3, false)
            }
            else{
              (main._1, received._2, main._3 + received._3, true)
            }
          }
        },
        triplet => {
          //Compute the messages to send
          if(triplet.srcAttr._1 >= triplet.dstAttr._1 && //Weight
             triplet.srcAttr._4 && !triplet.dstAttr._4){               //Dst is not finished
            //Send a message
            if(triplet.srcAttr._2 == 0){
              Iterator((triplet.dstId, (0, 1, 1, triplet.srcAttr._4)))
            }
            else{
              Iterator((triplet.dstId, (0, 0, 1, triplet.srcAttr._4)))
            }
          }
          else{
            //Don't send a message
            Iterator.empty
          }
        },
        (a,b) => {
          //Join messages into one
          (0, a._2 + b._2, a._3 + b._3, true)
        }
      )
      val totalMessages = sssp.vertices.reduce((a,b) => (0, (0, 0, a._2._3 + b._2._3, true)))
      println("TOTAL MESSAGES: " + totalMessages._2._3)
      val defeaterNodes = sssp.vertices.map(node => {
        if(node._2._2 > 0){
          0
        }
        else{
          1
        }
      }).reduce((a, b) => a+b)
      println("TOTAL DEFEATERS: " + defeaterNodes)
      defeaterNodes
    }
  }
