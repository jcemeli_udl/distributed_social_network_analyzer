//SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.DataFrame
import scala.collection.mutable.ListBuffer
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

class ParserCaller (session: SparkSession){
  def parseJson(id: String) : MPC = {
    val context = session.sparkContext
    val indexes = "Storage/Files/pairs-" + id + ".txt"
    val input = "Storage/Files/attributes-" + id + ".txt"
    val model = "Storage/Models/MPM"
    // val scriptPath = "parser/parser_all.py"
    // val scriptName = "parser_all.py"
    // context.addFile(scriptPath)
    // val data = context.parallelize(List(json_file))
    // val pipeRDD = data.pipe(scriptPath)
    // pipeRDD.collect()
    return new MPC(session, input, indexes, model)
  }
}
