//SparkContext
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

class TwitterDownloader (context: SparkContext){
  // def getGraphFromConversationId(id:String) : TwitterGraph = {
  //   val resultGraph : TwitterGraph = new TwitterGraph(context)
  //   val scriptPath = "Downloaders/twitter-collector/twcollect.py"
  //   val scriptName = "twcollect.py"
  //   context.addFile(scriptPath)
  //   val data = context.parallelize(List(id))
  //   val pipeRDD = data.pipe(scriptPath)
  //   pipeRDD.collect()
  //   resultGraph.parseConversationFromFile("Storage/Jsons/" + id + ".json")
  //   resultGraph
  // }

  def downloadAndStoreConversation(id: String) : String = {
    val scriptPath = "Downloaders/twitter-collector/twcollect.py"
    val scriptName = "twcollect.py"
    context.addFile(scriptPath)
    val data = context.parallelize(List(id))
    val pipeRDD = data.pipe(scriptPath)
    pipeRDD.collect()
    "Storage/Jsons/" + id + ".json"
  }
}
