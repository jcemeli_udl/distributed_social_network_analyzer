//SparkContext
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

class RedditDownloader (context: SparkContext){
  def getGraphFromConversationId(id:String, json_file:String) : RedditGraph = {
    val resultGraph : RedditGraph = new RedditGraph(context)
    val scriptPath = "Downloaders/reddit-collector/reddit.py"
    val scriptName = "reddit.py"
    context.addFile(scriptPath)
    val data = context.parallelize(List(id))
    val pipeRDD = data.pipe(scriptPath)
    pipeRDD.collect().foreach(println)
    resultGraph.parseRedditConversationFromFile(json_file)
    resultGraph
  }
}
