//SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.DataFrame
import scala.collection.mutable.ListBuffer
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

//GraphX TODO: TEST ONLY
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

//Lists
import scala.collection.mutable.ListBuffer

//Read files
import scala.io.Source

object MainApp{
  def main(args: Array[String]){
    val ss = SparkSession
      .builder
      .appName("Main Application")
      .getOrCreate()
    val sc = ss.sparkContext
    val path = "Storage/Jsons/" + args(0) + ".json"
    //Create MPC
    val parserCaller = new ParserCaller(ss)
    val classifier = parserCaller.parseJson(args(0))
    //Create graph
    val graph = new TwitterGraph(sc, classifier)
    graph.parseConversationFromFile(path)
    val calcs : RelationshipsCalcs = new RelationshipsCalcs()
    calcs.optimizedCalcDefeatersMessageCount(graph)
    ss.stop()
  }
}
