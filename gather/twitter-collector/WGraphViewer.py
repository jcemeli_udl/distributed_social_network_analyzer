
import Tkinter as tk

from PIL import Image
from PIL import ImageTk

class TwWGraphViewer(tk.Frame):
    def __init__(self, image):
      tk.Frame.__init__(self, master = tk.Toplevel())

      self.imagefile = image
      self.photo = Image.open(self.imagefile)
      self.ImageCanvas = tk.Canvas(master=self,bd=0, highlightthickness=0)
      self.Image = ImageTk.PhotoImage(self.photo)
      self.sprite = self.ImageCanvas.create_image(0,0,anchor=tk.NW,image=self.Image, tags="IMG")
      # self.ImageText = tk.Label(master=self,text=image)
      self.ImageCanvas.grid(row=0,sticky=tk.W+tk.E+tk.N+tk.S)
      # self.ImageText.grid(row=1)
      self.pack(fill=tk.BOTH, expand=1)
      self.bind("<Configure>", self.Resize)

    def Resize(self,event):
      size = (event.width,event.height)
      self.resized = self.photo.resize(size,Image.ANTIALIAS)
      self.Image = ImageTk.PhotoImage(self.resized)
      self.ImageCanvas.delete("IMG")
      self.ImageCanvas.create_image(0, 0, image=self.Image, anchor=tk.NW, tags="IMG")

    def quit(self):
      self.master.destroy()
