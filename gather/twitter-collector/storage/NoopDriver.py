# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''
Noop StorageDriver module.

A mockup to show how a storage driver must be writter.


Created on 12/10/2014

@author: carlesm
'''


from storage import *


class NoopDriver(StorageDriver):

    name = "Noop (Mock) StorageDriver"
    opcode = "noop"

    # DEPRECATED
    def debug_dump(self, *args, **kwargs):
        if self.debug:
            print "\t args: %s \n\t kwargs: %s" % (args, kwargs)

    @logfunction(logging.DEBUG)
    def __init__(self, *args, **kwargs):
        self.debug = kwargs.get("debug", False)
        if self.debug:
            consoleh.setLevel(logging.DEBUG)
        super(NoopDriver, self).__init__(*args, **kwargs)

    @logfunction(logging.DEBUG)
    def initialize(self, *args, **kwargs):
        return True

    @logfunction(logging.DEBUG)
    def store(self, obj):
        return True

    @logfunction(logging.DEBUG)
    def store_unique(self, obj):
        return True


    @logfunction(logging.DEBUG)
    def cleanup(self):
        return True
