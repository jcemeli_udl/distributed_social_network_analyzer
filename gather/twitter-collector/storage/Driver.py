# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''
Parent StorageDriver module.

Parent class for Storage  drivers.
Follows:
http://martyalchin.com/2008/jan/10/simple-plugin-framework/

Created on 12/10/2014

@author: carlesm
'''



class PluginMount(type):

    def __init__(cls, name, bases, attrs):
        if not hasattr(cls, 'plugins'):
            # This branch only executes when processing the mount point itself.
            # So, since this is a new plugin type, not an implementation, this
            # class shouldn't be registered as a plugin. Instead, it sets up a
            # list where plugins can be registered later.
            cls.plugins = []
        else:
            # This must be a plugin implementation, which should be registered.
            # Simply appending it to the list is all that's needed to keep
            # track of it later.
            cls.plugins.append(cls)

    def get_plugins(cls, *args, **kwargs):
        """
        Retrieve the registered plugins.
        """
        return [p(*args, **kwargs) for p in cls.plugins]

    def get_driver(cls, opcode):
        for plug in cls.plugins:
            if plug.opcode == opcode:
                return plug


class StorageDriver(object):

    """
    Mount point for Storage Drivers(plugins)

    Plugins implem. this reference should provide the following attributes:

    ========  ========================================================
    name      Name of the drivers
    opcode    Short Name used to load/instantiate it
    ========  ========================================================

    Plugins implementing this reference should provide the following methods:

    ========  ========================================================
    initialize  Init DB connection
    ========  ========================================================


    """
    __metaclass__ = PluginMount

    name = "Base StorageDriver"
    opcode = "base"

    def __init__(self, *args, **kwargs):
        pass

    def __str__(self):
        return self.name
