# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''
Mongo Storage Driver.

Mongo API wrapper for StorageDriver.


Created on 11/10/2014

@author: carlesm
'''

from storage import *
import pymongo
import logging


loglevel = logging.DEBUG  # logging.CRITICAL
loglev = logging.CRITICAL  # DEBUG
logger = logging.getLogger('TwCollect2')
# Configure logger to write to file/stdout etc, it's level etc
logger.setLevel(loglev)
consoleh = logging.StreamHandler()
consoleh.setLevel(loglev)
logger.addHandler(consoleh)


class MongoDriver(StorageDriver):

    name = "Mongo StorageDriver"
    opcode = "mongo"

    @logfunction2(logger, loglevel)
    def __init__(self, *args, **kwargs):
        super(MongoDriver, self).__init__(*args, **kwargs)
        pass
        # TODO: Must read args (dbname, collectio, debug)
        self.dbname = kwargs.get("db", "defaultdb")
        self.collname = kwargs.get("collection", "defaultcoll")
        self.host = kwargs.get("host", None)
        self.port = kwargs.get("port", None)
        self.url = kwargs.get("url", None)
        if not self.url:
            if not self.host:
                self.host = "localhost"
            if not self.port:
                self.port = "27017"
            self.url = "mongodb://%s:%s" % (self.host, self.port)

    @logfunction2(logger, loglevel)
    def initialize(self, *args, **kwargs):
        """
        Will init database connection, using provided kwargs
        or existing params or defaults
        """
        self.conn = pymongo.MongoClient(self.url)
        if "db" in kwargs:
            self.dbname = kwargs["db"]
        self.db = self.conn[self.dbname]
        if "collection" in kwargs:
            self.collname = kwargs["collection"]
        self.collection = self.db[self.collname]
        return True

    @logfunction2(logger, loglevel)
    def store(self, obj):
        """
        Store object in mongo. Object must be a python dict. Future releases
        will do more stuff with more data objects
        """
        self.collection.insert(obj)
        return True

    @logfunction2(logger, loglevel)
    def store_unique(self, obj, key):
        """
        Store object in mongo. Object must be a python dict. Future releases
        will do more stuff with more data objects
        """
        if key in obj:
            if not self.find_one(key, obj[key]):
                self.collection.insert(obj)
        return True

    @logfunction2(logger, loglevel)
    def find_one(self, key, value):
        return self.collection.find_one({key: value})

    @logfunction2(logger, loglevel)
    def find_all(self, key=None, value=None):
        if key:
            return self.collection.find({key: value})
        else:
            return self.collection.find()

    @logfunction2(logger, loglevel)
    def cleanup(self):
        return self.conn.close()


    # TODO
    # def drop() <- Drop Collectino
    # def update <- Change object? -> mongoversions
    # def delete <- Delete object
    # def add_to_object(key, value, data) <- Add items to obj
