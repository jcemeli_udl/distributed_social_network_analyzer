'''
    Storage drivers


    Includes:
        mongodb

@__author__ : carlesm
@author: carlesm
'''


import sys
if sys.version_info[:2] < (2, 6):
    m = "Python version 2.6 or later is required to work (%d.%d detected)."
    raise ImportError(m % sys.version_info[:2])
del sys

# Release

__author__ = 'Carles Mateu <carlesm@carlesm.com>'
__license__ = "License :: OSI Approved :: GPL License"
__date__ = "Nov 2014"
__version__ = "0.0.1"


from utils import *

# import logging
# loglev = logging.CRITICAL
# logger = logging.getLogger('TraceLog')
# # # Configure logger to write to file/stdout etc, it's level etc
# logger.setLevel(loglev)
# consoleh = logging.StreamHandler()
# consoleh.setLevel(loglev)
# logger.addHandler(consoleh)


# def logfunction(level):
#     def _decorator(fn):
#         def _decorated(*arg, **kwargs):
#             logger.log(level, "calling '%s'(%r,%r)", fn.func_name, arg, kwargs)
#             ret = fn(*arg, **kwargs)
#             logger.log(level, "called '%s'(%r,%r) got return value: %r",
#                        fn.func_name, arg, kwargs, ret)
#             return ret
#         return _decorated
#     return _decorator


# these packages work with Python >= 2.6

from Driver import StorageDriver
from MongoDriver import MongoDriver
from NoopDriver import NoopDriver






__all__ = ["StorageDriver", "MongoDriver", "NoopDriver"]
