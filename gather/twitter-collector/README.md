
# twcollect usage

twcollect is a python script to gather data from twitter.

Usage:

## Commands to get collections of tweets:

### Searching tweets with a word in:

```
 python gather/twitter-collector/twcollect.py -s -a ramontweets -c esperanzaaguirre -x 2000 search_tweets_word esperanzaaguirre  ;
 python gather/twitter-collector/twcollect.py -s -a ramontweets -c montoro -x 2000 search_tweets_word montoro ;
 python gather/twitter-collector/twcollect.py -s -a ramontweets -c deguindos -x 2000 search_tweets_word deguindos ;
 python gather/twitter-collector/twcollect.py -s -a ramontweets -c rajoy -x 2000 search_tweets_word rajoy
 python gather/twitter-collector/twcollect.py -s -a ramontweets -c Wert -x 2000 search_tweets_word wert
```

### Getting lists of tweets by a given user screen name:

```
gather/twitter-collector/twcollect.py --urls  get_tweets_by_screen HuffingtonPost > HuffingtonPostURLs.txt
```

collections on CarlesMongoDB:
dbname:  ramontweets
collections (get them with db.getCollectionNames()):

### Get a conversation


```sh
$ python twcollect.py  get_conversation_by_id 558229471871119360
```

will retrieve the conversation (searching it through to: references on tweets) initiated by tweet_id 558229471871119360

###  Get tweets from conversations from a list with  tweet status URLs representing the root of those conversations

```sh
$ ./getIDsfromtwitterconv.py convURList.txt  user password minconvsize
```

will check, for every twitter status URL inside convURList.txt whether its
associated conversation (accessed through firefox), has at least minconvsize. If
that is the case, it will send all the tweet ids of the conversation in a
file (one conversation file for each conversation).

then, we can use:


```sh
$  twitter-collector/twcollect.py -o get_tweets_fromlist conversationfile  > result.js
```

to get all the tweets (json objects) from the conversationfile


### Conversation gathering for large amounts of conversations

We, first, download the tweet id's that are part of the conversation:

```sh
$  python twcollect.py retrieve_conversations_from_list -s -a OT2R2 -c conversations 3.URLs
```

This will download the tweets from all conversations in 3.URLs file, either IDs or URLs, into
a collection called [conversations] into DB [OT2R2].

We can then download all tweets pertaining to a conversation (790311671310974977, can be
an URL): into collection [tweets] (by default, can be changed with '--tweets'). By default conversation information is in a collection
called [conversations], can be changed with '--conversations <collection>'

```sh
$  python twcollect.py get_tweets_from_conversation -s -a OT2R2 -c tweets 790311671310974977
```

Or we can download all tweets from a list of conversations:

```sh
$  python twcollect.py get_tweets_from_conversation_list -s -a OT2R2 -c tweets 3.URLs
```

In this case 3.URLs is a file of either IDs or URLs for conversations. Information for conversations
will be retrieved from collection [conversations] by default (unless '--conversations <collection>'
specifies another thing).
