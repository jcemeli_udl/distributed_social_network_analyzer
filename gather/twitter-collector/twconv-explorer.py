#!/usr/bin/env python
#
# Author: Ramon Bejar
#
# Tkinker infor mainly from: http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/index.html
#

import Tkinter as tk
import subprocess
import time
import sys
import os
from threading import Thread

import getIDsfromtwitterconv
import twcollect
from socialarg.argumentation.valuedAF import showResultDigraph as showResultDigraph

import WGraphViewer

class TwDownLoaderApplication(tk.Frame):
    def __init__(self, master=None,login=None,password=None,dbname='ramontweets'):
        self.wsourcesuffix = ['followers_count','retweet_count','favorite_count']
        self.propsuffix = ['noprop','sum_supports','max_supports']
        tk.Frame.__init__(self, master)
        self.classifyproc = None
        self.grid(ipadx=4,ipady=4,padx=4,pady=4)
        self.XMLOptValue = tk.IntVar()
        self.XMLWSourceOptValue = tk.IntVar()
        self.XMLPropOptValue = tk.IntVar()
        self.returnpressed = 0
        self.login=login
        self.password=password
        self.XMLFileName = ""
        self.dbname = dbname
        self.COLLECTERPATH = os.environ.get('SOCIALARGTWCOLLECTER')
        if (self.COLLECTERPATH == None):
            self.COLLECTERPATH = './'
        self.DBMGRPATH = os.environ.get('SOCIALARGDBMGR')
        if (self.DBMGRPATH == None):
            self.DBMGRPATH = ''
        self.ownpath = os.getcwd()
        print "Paths: ", self.ownpath, " ", self.COLLECTERPATH, " ", self.DBMGRPATH
        self.listofGraphs = []
        self.createWidgets()
        self.SaveToXMLChanged()

    def quit(self):
        self.master.destroy()

    def showSourceGroup(self):
        self.urlLabel = tk.Label(self.ConvSourceGroup, text="Conversation URL: ")
        self.emptyLabel = tk.Label(self.ConvSourceGroup, text="                  ")
        self.urleditbox = tk.Entry(self.ConvSourceGroup)
        self.convdownload = tk.Button(self.ConvSourceGroup, text='Download Conv', command=self.downloadConversation)
        # Place on the group grid:
        self.urlLabel.grid(row=0,column=0)
        self.urleditbox.grid(row=1,column=0)
        self.emptyLabel.grid(row=2,column=0)
        self.convdownload.grid(row=3,column=0)

    def showDatabaseGroup(self):
        self.convDBcollectnameLabel = tk.Label(self.DatabaseGroup, text="Conv collection: ")
        self.classifyDBcollectnameLabel = tk.Label(self.DatabaseGroup,  text="Conv classify collection: ")
        self.convDBcollectname = tk.Entry(self.DatabaseGroup,width=50)
        self.classifyDBcollectname = tk.Entry(self.DatabaseGroup,width=50)

        self.saveToXMLOption = tk.Radiobutton(self.DatabaseGroup,text="Save to XML", variable=self.XMLOptValue,
            value=1, command=self.SaveToXMLChanged)

        self.DontSaveToXMLOption = tk.Radiobutton(self.DatabaseGroup,text="Do NOT Save to XML", variable=self.XMLOptValue,
            value=0, command=self.SaveToXMLChanged)


        self.FollowersXMLOption = tk.Radiobutton(self.DatabaseGroup,text="Followers Weight", variable=self.XMLWSourceOptValue ,
            value=0, command=self.SaveToXMLChanged)

        self.RetweetsXMLOption = tk.Radiobutton(self.DatabaseGroup,text="Retweets Weight", variable=self.XMLWSourceOptValue ,
             value=1, command=self.SaveToXMLChanged)

        self.FavoritesXMLOption = tk.Radiobutton(self.DatabaseGroup,text="Favorites Weight", variable=self.XMLWSourceOptValue ,
             value=2, command=self.SaveToXMLChanged)


        self.NoPropXMLOption = tk.Radiobutton(self.DatabaseGroup,text="No Support Prop.", variable=self.XMLPropOptValue  ,
            value=0, command=self.SaveToXMLChanged)

        self.PropSumXMLOption = tk.Radiobutton(self.DatabaseGroup,text="Sum Support Prop.", variable=self.XMLPropOptValue  ,
           value=1, command=self.SaveToXMLChanged)

        self.PropMaxXMLOption = tk.Radiobutton(self.DatabaseGroup,text="Max Support Prop.", variable=self.XMLPropOptValue ,
            value=2, command=self.SaveToXMLChanged)

        self.xmleditboxLabel = tk.Label(self.DatabaseGroup,  text="XML File: ")
        self.xmleditbox = tk.Entry(self.DatabaseGroup,width=30)
        self.xmlsuffixLabel = tk.Label(self.DatabaseGroup, text=self.getXMLSuffix())

        self.saveToDatabase = tk.Button(self.DatabaseGroup, text='Save to DB', command=self.saveToDB)
        self.saveAndClassifyReplies = tk.Button(self.DatabaseGroup, text='Save to DB and Classify Replies',
            command=self.classifyReplies)
        self.saveAndClassifyRepliesMentions = tk.Button(self.DatabaseGroup, text='Save to DB and Classify Replies & Mentions',
            command=self.classifyRepliesAndMentions)
        self.TargetTweetS = tk.Label(self.DatabaseGroup,text="Target tweet: ",width=90,height=2,justify='left')
        self.TargetTweet = tk.Label(self.DatabaseGroup,text="",width=140,height=3,bd=4,relief=tk.GROOVE,justify='left')
        self.ReplyTweetS = tk.Label(self.DatabaseGroup,text="Reply/Answer tweet: ",width=90,height=2,justify='left')
        self.ReplyTweet = tk.Label(self.DatabaseGroup,text="",width=140,height=3,bd=4,relief=tk.GROOVE,justify='left')
        self.Actions = tk.Label(self.DatabaseGroup,text="Actions:  ",width=100,height=2)
        self.saveAndNext = tk.Entry(self.DatabaseGroup,width=4 )
        self.saveAndNext.bind('<Return>',self.returncallback)
        # Place on the group grid:
        self.convDBcollectnameLabel.grid(row=0,column=0)
        self.classifyDBcollectnameLabel.grid(row=1,column=0)
        self.convDBcollectname.grid(row=0,column=1)
        self.classifyDBcollectname.grid(row=1,column=1)

        self.saveToXMLOption.grid(row=2,column=0)
        self.DontSaveToXMLOption.grid(row=2,column=1)

        self.FollowersXMLOption.grid(row=3,column=0)
        self.RetweetsXMLOption.grid(row=3,column=1)
        self.FavoritesXMLOption.grid(row=3,column=2)
        self.NoPropXMLOption.grid(row=4,column=0)
        self.PropSumXMLOption.grid(row=4,column=1)
        self.PropMaxXMLOption.grid(row=4,column=2)

        self.xmleditboxLabel.grid(row=5,column=0)
        self.xmleditbox.grid(row=5,column=1,sticky='E')
        self.xmlsuffixLabel.grid(row=5,column=2,sticky='W')

        self.saveToDatabase.grid(row=6,column=0,columnspan=1)

        self.saveAndClassifyReplies.grid(row=6,column=1,columnspan=1)
        self.saveAndClassifyRepliesMentions.grid(row=6,column=2,columnspan=1)
        self.TargetTweetS.grid(row=8,column=0,columnspan=2,
                               ipadx=2,ipady=0,padx=2,pady=0)
        self.TargetTweet.grid(row=9,column=0,columnspan=3,rowspan=2,sticky='W',
                               ipadx=2,ipady=0,padx=2,pady=2)
        self.ReplyTweetS.grid(row=12,column=0,columnspan=2,
                                ipadx=2,ipady=0,padx=2,pady=0)
        self.ReplyTweet.grid(row=13,column=0,columnspan=3,rowspan=2,sticky='W',
                                 ipadx=2,ipady=0,padx=2,pady=2)
        self.Actions.grid(row=16,column=0,columnspan=2,sticky='WEN')
        self.saveAndNext.grid(row=16,column=2)


    def showDrawGroup(self):
        # pass
        #self.drawFoDiscussionGraph = tk.Button(self.DrawGroup, text='Draw FollowersW Graph',
        #    command=self.drawWFollowersGraph)
        self.drawWDiscussionGraph = tk.Button(self.DrawGroup, text='Draw W Discussion Graph',
            command=self.drawWDisGraph)
        #self.drawFaDiscussionGraph = tk.Button(self.DrawGroup, text='Draw FavoritesW Graph',
        #        command=self.drawWFavoritesGraph)

        #self.drawFoDiscussionGraph.grid(row=0,column=0,columnspan=1,ipadx=2,ipady=2,padx=2,pady=2)
        self.drawWDiscussionGraph.grid(row=0,column=1,columnspan=1,ipadx=2,ipady=2,padx=2,pady=2)
        #self.drawFaDiscussionGraph.grid(row=0,column=2,columnspan=1,ipadx=2,ipady=2,padx=2,pady=2)

    def downloadConversation(self):
        self.twdownloader = getIDsfromtwitterconv.TwconcDownloader(self.login , self.password )
        tweetlist = self.twdownloader.downloadConversation(self.urleditbox.get())
        if (tweetlist == []):
            return
        print tweetlist
        f = open( self.convDBcollectname.get()+'.ids', 'w')
        for tw in tweetlist:
            f.write(str(tw)+'\n')
        f.close()
        #twcollecter = twcollect.TwCollect()
        #opts = twc.parse(sys.argv)
        #twc.init()
        #twc.run()
        cmd = [ self.COLLECTERPATH+"/twcollect.py", "-o", "get_tweets_fromlist", self.convDBcollectname.get()+".ids" ]
        print " Executing : ",cmd
        twcollecter = subprocess.Popen( cmd, stdout=subprocess.PIPE, cwd=self.COLLECTERPATH)
        f = open( self.convDBcollectname.get()+'.json', 'w')
        line = twcollecter.stdout.readline()
        while (line != ''):
            print "---",line,"---"
            f.write(line+'\n')
            line = twcollecter.stdout.readline()
        f.close()

    def saveToDB(self):
        cmd = self.DBMGRPATH+'/data2xml.py'
        cmdargs = '-i '+ self.ownpath+'/'+self.convDBcollectname.get()+'.json'
        cmdargs = cmdargs+ ' -qt all --db '+self.dbname+' --collection '+self.convDBcollectname.get()
        cmdargs = cmdargs + ' -ws '+self.wsourcesuffix[self.XMLWSourceOptValue.get()]
        cmdargs = cmdargs + ' -ct only_reply_to '
        if (self.propsuffix[self.XMLPropOptValue.get()] != 'noprop'):
            cmdargs = cmdargs + ' --relevance_propagation '+self.propsuffix[self.XMLPropOptValue.get()]
        if (self.XMLOptValue.get() == 1):
           cmdargs = cmdargs + ' -o '+self.ownpath+'/'+self.XMLFileName
        print ' Executing : '+cmd+" "+cmdargs
        dbcollecter = subprocess.Popen( [ cmd ]+cmdargs.split(), cwd=self.DBMGRPATH )
        dbcollecter.wait()

    def getXMLSuffix( self ):
        suffix = "-"+self.wsourcesuffix[self.XMLWSourceOptValue.get()]+"-"+self.propsuffix[self.XMLPropOptValue.get()]+".xml"
        return suffix

    def SaveToXMLChanged(self):
        # self.saveToDatabase.configure(text='Save to DB')
        suffix = self.getXMLSuffix()
        self.xmlsuffixLabel.configure(text=suffix)
        self.XMLFileName = self.xmleditbox.get()+suffix
        print "New XML File Name : ", self.XMLFileName

    def classifyReplies(self):
        pass

    def classifyRepliesAndMentions(self):
        # pass
        # self.FakeClassifyLoop()
        if ((self.classifyproc is None) or (self.classifyproc.returncode is not None)):
            self.saveAndNext.delete(0,tk.END)
            print "-- Starting new classify R&M process loop --"
            cmd = self.DBMGRPATH+'/data2xml.py'
            cmdargs = '-i '+ self.ownpath+'/'+self.convDBcollectname.get()+'.json'
            cmdargs = cmdargs+ ' -qt all --db '+self.dbname+' --collection '+self.convDBcollectname.get()
            cmdargs = cmdargs + ' -ws '+self.wsourcesuffix[self.XMLWSourceOptValue.get()]
            cmdargs = cmdargs + ' -ct replies_and_mentions'
            cmdargs = cmdargs + ' --classify --classify_collection '+self.classifyDBcollectname.get()
            if (self.propsuffix[self.XMLPropOptValue.get()] != 'noprop'):
                cmdargs = cmdargs + ' --relevance_propagation '+self.propsuffix[self.XMLPropOptValue.get()]
            if (self.XMLOptValue.get() == 1):
               cmdargs = cmdargs + ' -o '+self.ownpath+'/'+self.XMLFileName
            print ' Executing : '+cmd+" "+cmdargs
            self.classifyproc = subprocess.Popen( [ cmd ]+cmdargs.split(),
                          stdin=subprocess.PIPE, stdout=subprocess.PIPE, cwd=self.DBMGRPATH )
            self.readerth = Thread(target=self.reader_thread)
            self.readerth.start()

    def reader_thread(self):
        line = self.classifyproc.stdout.readline()
        while (line != ''):
            # print "---",line,"---"
            if (line.startswith('Post ')):
              line2 = self.classifyproc.stdout.readline()
              self.TargetTweetS.configure(text=line)
              self.TargetTweet.configure(text=line2)
            if (line.startswith('Reply ')):
              line2 = self.classifyproc.stdout.readline()
              self.ReplyTweetS.configure(text=line)
              self.ReplyTweet.configure(text=line2)
              time.sleep(0.5)
            if (line.startswith('Manual classification ')):
                  toks = line.split()
                  self.Actions.configure(text=' '.join(toks[2:]))
            line = self.classifyproc.stdout.readline()
        self.classifyproc.poll()
        self.saveAndNext.delete(0,tk.END)

    def returncallback(self,event):
        if (self.classifyproc is not None):
            self.classifyproc.poll()
            if (self.classifyproc.returncode is None):
                self.classifyproc.stdin.write(self.saveAndNext.get()+'\n')
                self.saveAndNext.delete(0,tk.END)
            else:
                print "Classification ended"
        else:
            print "No classification running"

    def drawWDisGraph( self ):
        DigraphBuilder = showResultDigraph.ShowResultDigraph( self.XMLFileName,
                          ( 'original' if (self.XMLWSourceOptValue.get() == 0) else 'propagated') )
        # if (len(sys.argv) > 7):
        #    minW = ( sys.argv[6] if (sys.argv[6] != "-") else None )
        # maxW = ( sys.argv[7] if (sys.argv[7] != "-") else None )
        DigraphBuilder.createSupportAttackDigraph( None, "print-gv", "short", True, None, None )
        graphimagefile = self.XMLFileName+".gv.png"
        self.listofGraphs.append(WGraphViewer.TwWGraphViewer(image=graphimagefile))

    #def drawWRetweetsGraph( self ):
    #    pass

    #def drawWFavoritesGraph( self ):
    #    pass

    def createWidgets(self):
        self.ConvSourceGroup = tk.Frame(self,bd=4,relief=tk.GROOVE)
        self.DrawGroup = tk.Frame(self,bd=4,relief=tk.GROOVE)
        self.DatabaseGroup = tk.Frame(self,bd=4,relief=tk.GROOVE)


        self.ConvSourceGroup.grid(row=0,column=0,ipadx=2,ipady=2)
        self.DatabaseGroup.grid(row=0,column=1,ipadx=2,ipady=2)
        self.DrawGroup.grid(row=2,column=1,ipadx=2,ipady=2)
        self.showSourceGroup()
        self.showDatabaseGroup()
        self.showDrawGroup()

        self.quitButton = tk.Button(self, text='Quit',command=self.quit)
        self.quitButton.grid(row=12,column=0)

if (__name__ == '__main__'):
    app = TwDownLoaderApplication(login=sys.argv[1],password=sys.argv[2])
    app.master.title('Twitter Conversation Downloader')
    app.mainloop()
