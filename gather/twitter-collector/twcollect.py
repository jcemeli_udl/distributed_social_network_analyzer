#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''
Originally Created on 01/10/2012
Rebootstrapped 01/22/2015

@author: carlesm
'''


import optparse
import sys
import json
import dcoll
import storage

import requests
import urlparse
import logging

from utils import *
from tqdm import tqdm

import re


try:
    from html import unescape  # python 3.4+
except ImportError:
    try:
        from html.parser import HTMLParser  # python 3.x (<3.4)
        unescape = HTMLParser().unescape
    except ImportError:
        from HTMLParser import HTMLParser  # python 2.x
        unescape = HTMLParser().unescape


import urllib3
import urllib3.contrib.pyopenssl
urllib3.disable_warnings()
urllib3.contrib.pyopenssl.inject_into_urllib3()
logging.captureWarnings(True)


class TwCollect(object):

    def __init__(self):
        pass

    def parse_args(self):
        """Will parse options on cmd line"""

        p = optparse.OptionParser()

        grpdb = optparse.OptionGroup(p, "Database Options",
                                     "Storage/database related options.")

        # grpdb.add_option('-b', '--dbdriver', action='store', dest='dbdriver',
        #             help='Database Driver')

        grpdb.add_option('-a', '--database', action='store', dest='dbname',
                         help='Database Name')

        grpdb.add_option('-c', '--coll', action='store', dest='dbcollection',
                         help='Database Collection')

        grpdb.add_option('-s', '--store', action='store_true', dest='store',
                         help='Store results in DB')

        grpdb.add_option('-y', '--dyn-store', action='store_true',
                         dest='dynstore', help='Store results dinamically, "\
                         "can result in partial storage. Not all commands "\
                         "support it')

        grpdb.add_option('--conversations', action='store', dest='convs',
                         help='Conversations collection')

        grpdb.add_option('--tweets', action='store', dest='tweets',
                         help='Conversations collection')

        p.add_option_group(grpdb)

        grpauth = optparse.OptionGroup(p, "Auth Options",
                                       "Oauth/Auth/Login options.")
        grpauth.add_option('-u', '--oauth', action='store', dest='oauthf',
                           help='Oauth File')
        p.add_option_group(grpauth)

        grpout = optparse.OptionGroup(p, "Output Options",
                                      "Output control options.")
        grpout.add_option('-o', '--output',  action='store_true',
                          dest='output',
                          help='Output to screen ')

        grpout.add_option('-i', '--tweetids',  action='store_true',
                          dest='tweetids',
                          help='Only show tweetids ')

        grpout.add_option('-r', '--readable',  action='store_true',
                          dest='readable',
                          help='Readable text output file (JSON/XML)')

        grpout.add_option('-x', '--maxamount',  action='store',
                          dest='max_amount', type="int",
                          help='Limit amount of output results.')

        grpout.add_option('--urls',  action='store_true',
                          dest='showurls',
                          help='print the status URLs of the resulting "\
                          "tweets, not their json objects')

        p.add_option_group(grpout)

        # DEBUG OPTIONS
        grpdbg = optparse.OptionGroup(
            p, "Debug Options", "Debug control options.")

        grpdbg.add_option('-l', '--log', action='store', dest='logfile',
                          help='Log to file')

        grpdbg.add_option('-d', '--debug', action='store', type='choice',
                          choices=[
                              "CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
                          help='''Logging level:          '''
                               '''CRITICAL, ERROR, WARNING, INFO, DEBUG''')
        p.add_option_group(grpdbg)

        p.set_defaults(
            logfile=None,
            debug="CRITICAL",
            readable=False,
            oauthf="twcollect.oauth",
            max_amount=None,
            dbname="twcollect",
            dbcollection="tweets",
            store=False,
            dynstore=False,
            output=False,
            tweetids=False,
            convs="conversations",
            tweets="tweets",
        )

        options, args = p.parse_args()
        self.optparse = p
        return options, args

    def init_twitter(self):
        self.twsource = dcoll.twit()

        # twsource.setAuth() <- set auth data
        # twsource.fetchData() <- get data
        # while twsource.cursor()
        #    db.store()

        #    CARLESM
        #    APP_NAME = 'twtour'
        #    CONSUMER_KEY = 'SzhFl1tIaeuwgfgugSx2Pw'
        #    CONSUMER_SECRET = '5FRDk2z3rvIErI3uHARQMnt1N7FJMmgZ2z7ASRMjM'

        # CCIA2011
        APP_NAME = 'twtour01'
        CONSUMER_KEY = 'yIDPUaYIxSidu0oTVmN86A'
        CONSUMER_SECRET = 'X8AtcollGGergCDjGXkvYkalPUuczGlJCOh6dXrF7w'
        self.twsource.set_auth(APP_NAME, CONSUMER_KEY,
                               CONSUMER_SECRET, self.options.oauthf)
        self.twsource.auth()

    def get_tweet_id(self, args):
        self.init_twitter()
        return self.twsource.get_single_tweet(args[0])

    def get_tweets_by_refer(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "to:%s" % (args[0])
        KW['since_id'] = args[1]
        search_result = self.twsource.search_tweets(KW)
        tweets = search_result['statuses']
        return tweets

    # get all tweets where the screen name @args[0] appears
    def get_tweets_by_refer2(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "@%s" % (args[0])
        KW['since_id'] = args[1]
        return self.twsource.search_tweets_paginated(KW)
        # tweets = search_result['statuses']
        # return tweets

    def get_conversation_by_id_removed_code(self, args):
        self.init_twitter()
        orig_tweet = self.twsource.get_single_tweet(args[0])
        refered_to = orig_tweet['user']['screen_name']
        tweet_id = orig_tweet['id']
        response = [orig_tweet]
        for i in self.get_tweets_by_refer([refered_to, tweet_id]):
            if i['in_reply_to_status_id'] == tweet_id:
                response.append(i)
        # for r in response:
        #    print json.dumps(r)
        return response

    def _clean_page(self, page):
        newtext = page  # .encode('utf-8')
        newtext = newtext.replace('\\n', '\n')
        newtext = unescape(newtext)
        newtext = newtext.replace('\\u003c', '<')
        newtext = newtext.replace('\\u003e', '>')
        newtext = newtext.replace('\\/', '/')
        newtext = newtext.replace('\\\"', '\"')
        return newtext

    def _get_conversation_ids(self, username, twtid, tweets):
        # repeat:
        #   Get html
        #   Get tweetids from HTML/HTML_FRAGMENT_IN_JSON
        #   recurse to tweetids
        #   if ! data-min-position in HTML
        #       break
        repeat = True
        url = "https://twitter.com/%s/status/%s" % (str(username), str(twtid))
        reg = re.compile(r'<a href=\"(/[^/]*/status/[0-9]*)\" class=\"tweet-timestamp js-permalink js-nav js-tooltip\"')
        rega = re.compile(r'data-min-position=\"([^\"]*)\"')
        regb = re.compile(r'\"min_position\":\"([^\"]*)\"')
        convbase = "https://twitter.com/i/"
        convargs = "?include_available_features=1&include_entities=1&"\
                   "max_position="
        nextt = ""
        while repeat:
            repeat = False
            oldnext = nextt
            page = requests.get(url).text
            page = self._clean_page(page)
            items = reg.findall(page)
            for it in items:
                s1, username2, s2, twtid2 = it.split('/')
                if twtid2 not in tweets:
                    tweets.append(twtid2)
                    self._get_conversation_ids(username2, twtid2, tweets)
            m1 = rega.search(page)
            if m1:
                nextt = m1.group(1)
            else:
                m2 = regb.search(page)
                if m2:
                    nextt = m2.group(1)
            if nextt and nextt is not oldnext:
                url = "%s%s/conversation/%s%s%s" % (convbase, str(username),
                                                    str(twtid), convargs,
                                                    nextt)
                repeat = True

    def _get_conversation_tree_by_id(self, tweetid):
        try:
            orig_tweet = self.twsource.get_single_tweet(tweetid)
        except Exception as e:
            print e, tweetid
            return []
        username = orig_tweet['user']['screen_name']
        tweetids = []
        self._get_conversation_ids(username, tweetid, tweetids)
        return tweetids

    def get_conversation_by_id(self, args):
        self.init_twitter()
        reg = re.compile(r'https://twitter.com/.+/status/([0-9]*)')
        if "http" in args[0]:
            args[0] = reg.findall(args[0])[0]
        tweetids = self._get_conversation_tree_by_id(args[0])
        response = []
        for twtid in tweetids:
            resp = self.twsource.get_single_tweet(twtid)
            if resp:
                response.append(resp)
        return response

    def get_conversations_from_list(self, args):
        # for i in list:
        #   if i is URL:
        #       extract id
        #   get_conversation_by_id(i)
        #  if store:
        #    store_results
        # return results ?
        reg = re.compile(r'https://twitter.com/.+/status/([0-9]*)')
        responsed = {}
        response = []
        f = open(args[0], "r")
        for line in tqdm(f):
            if "http" in line:
                idtweet = reg.findall(line)[0]
            tweets = self._get_conversation_tree_by_id(idtweet)
            for t in tweets:
                responsed[t] = responsed.get(t, 0) + 1
                if t not in response:
                    response.append(t)
        f.close()
        total = 0.0
        maximum = len(responsed)
        maxim05 = maximum * 0.5
        for k in responsed:
            if k >= maxim05:
                print responsed[k], k
            total += float(responsed[k])
        print maximum, total, total/maximum
        return response

    def retrieve_conversations_from_list(self, args):
        """
        Returns conversations (for storage) from a list of IDs or URLs

        Format:
        [
            { conversation_id,
              root_id,
              teets: [ids, ...]
              length
            }
        ]
        """
        # for i in list:
        #   if i is URL:
        #       extract id
        #   get_conversation_by_id(i)
        #  if store:
        #    store_results
        # return results ?
        self.init_twitter()
        reg = re.compile(r'https://twitter.com/.+/status/([0-9]*)')
        response = []
        f = open(args[0], "r")
        for line in tqdm(f):
            if "http" in line:
                idtweet = reg.findall(line)[0]
            else:
                idtweet = line
            tweets = self._get_conversation_tree_by_id(idtweet)
            if tweets and len(tweets) > 0:
                conversation = {}
                conversation["root_id"] = tweets[0]
                conversation["length"] = len(tweets)
                conversation["tweets"] = tweets
                conversation["conversation_id"] = idtweet
                response.append(conversation)
        f.close()
        return response

    def _get_tweets_from_conversation(self, args):
        conv = self.conv_storage.find_one("conversation_id", args[0])
        if conv:
            tweets = conv["tweets"]
            response = []
            for t in tweets:
                resp = self.twsource.get_single_tweet(t)
                # TODO: add dyn-store support
                if resp:
                    response.append(resp)
        else:
            response = []
        return response

    def get_tweets_from_conversation(self, args):
        self.init_twitter()
        return self._get_tweets_from_conversation(args)

    def get_tweets_from_conversation_list(self, args):
        # for i in list:
        #   if i is URL:
        #       extract id
        #   get_tweets_from_conversation(i)
        # return results ?
        self.init_twitter()
        reg = re.compile(r'https://twitter.com/.+/status/([0-9]*)')
        response = []
        f = open(args[0], "r")
        for line in tqdm(f):
            if "http" in line:
                idtweet = reg.findall(line)[0]
            else:
                idtweet = args[0]
            tweets = self.get_tweets_from_conversation([idtweet])
            for t in tweets:
                # TODO: add dyn-store support
                if t not in response:
                    response.append(t)
        f.close()
        return response

    def generate_collection_from_conversation(self, args):
        #
        # Select conversation
        # for t in tweets:
        #   add result <- tweets
        # return result
        conv = self.conv_storage.find_one("conversation_id", args[0])
        response = []
        if conv:
            tweets = conv["tweets"]
            for t in tweets:
                tweet = self.tweet_storage.find_one("id_str", t)
                if tweet:
                    # print tweet["id"], tweet["user"]["name"]
                    response.append(tweet)
        return response

    def get_subtree_by_tweet(self, tw):
        # store sub-root
        orig_tweet = tw
        refered_to = orig_tweet['user']['screen_name']
        tweet_id = orig_tweet['id']
        subtree = [orig_tweet]
        # print "getting subtree for ", tweet_id, " screen name", refered_to
        # search child nodes
        for i in self.get_tweets_by_refer2([refered_to, tweet_id]):
            # print "child tweet id", i['id'], "in reply to"\
            #       ": ", i['in_reply_to_status_id']
            if i['in_reply_to_status_id'] == tweet_id:
                subtree += self.get_subtree_by_tweet(i)

        return subtree

        # recursively get a conversation tree
    def get_conversationtree_by_id(self, args):
        self.init_twitter()
        # get root tweet
        orig_tweet = self.twsource.get_single_tweet(args[0])
        refered_to = orig_tweet['user']['screen_name']
        tweet_id = orig_tweet['id']
        # print "getting tree for ", args[0], " screen name", refered_to
        tree = [orig_tweet]
        # search child nodes
        for i in self.get_tweets_by_refer2([refered_to, tweet_id]):
            # print "child tweet id", i['id'], "in reply to: ", \
            # i['in_reply_to_status_id']
            if i['in_reply_to_status_id'] == tweet_id:
                tree += self.get_subtree_by_tweet(i)

        return tree

    def get_tweets_fromlist(self, args):
        self.init_twitter()
        response = []
        f = open(args[0], "r")
        for line in f:
            toks = line.split()
            try:
                resp = self.twsource.get_single_tweet(toks[0])
                if resp is not None:
                    response.append(resp)
            except:
                pass
        f.close()
        return response

    def get_tweets_fromURLlist(self, args):
        self.init_twitter()
        response = []
        f = open(args[0], "r")
        for line in f:
            # print line
            urltweet = urlparse.urlparse(line)
            if (urltweet.path != ''):
                toks = urltweet.path.split('/')
                # print toks
                # print toks[-1]
                # size = len(toks)
            response.append(self.twsource.get_single_tweet(toks[-1].strip()))
        f.close()
        return response

    def print_commands(self, args):
        print "Available commands:"
        for i in self.cmds:
            print "\t", i
        self.optparse.print_help()

    def get_tweets_by_screen(self, args):
        self.init_twitter()
        tws = []
        twitcnt = 1
        for i in self.twsource.get_next_timeline(args[0], rts=True):
            tws.append(i)
            twitcnt += 1
            if self.options.max_amount and twitcnt > self.options.max_amount:
                break
        return tws

    def search_tweets_word_list(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "%s" % (args[0])
        # KW['since_id'] = args[1]
        tweets = list(self.twsource.search_tweets_paginated(KW))
        return tweets

    def printresult(self, *args):
        for arg in args:
            if not isinstance(arg, list):
                arg2 = [arg]
            else:
                arg2 = arg
            for itm in arg2:
                if self.options.tweetids is True:
                    print itm["id"]
                elif self.options.readable:
                    print json.dumps(itm, indent=4)
                else:
                    print json.dumps(itm)

    def printURLs(self, *args):
        for arg in args:
            if not isinstance(arg, list):
                arg2 = [arg]
            else:
                arg2 = arg
            for itm in arg2:
                if (self.options.showurls):
                    if (self.options.showurls is True):
                        print "https://twitter.com/" + \
                            itm['user']['screen_name'] + \
                            "/status/"+str(itm['id'])

    def search_tweets_word(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "%s" % (args[0])
        # if not self.options.store and self.options.output:
        # if Storing there's no maximum
        # TODO: Must add option to screen out (the limited one)
        # if self.options.screenout
        #     if not self.options.max_amount
        #           or self.options.max_amount > 10000:
        # return [{"error": "too much items for screen or not limited"}]
        numit = 0
        for tw in self.twsource.search_tweets_paginated(KW):
            if self.options.store:
                self.storage.store_unique(tw, "id")
                # self.storage.store(tw) #, "id")
            if self.options.output:
                self.printresult(tw)
            else:
                if (self.options.showurls):
                    if (self.options.showurls is True):
                        print "https://twitter.com/" + \
                              tw['user']['screen_name'] + \
                              "/status/"+str(tw['id'])

            numit += 1
            if self.options.max_amount:
                if numit >= self.options.max_amount:
                    break
        print >> sys.stderr, "TOT: ", numit
        return None

    def search_tweets(self, args):
        self.init_twitter()
        # KW = {}
        # Args will be a dict JSON style (as of now)
        KW = json.loads(args[0])
        # print KW
        # KW['q'] = "%s" % (args[0])
        # if not self.options.store and self.options.output:
        # if Storing there's no maximum
        # TODO: Must add option to screen out (the limited one)
        # if self.options.screenout
        #     if not self.options.max_amount
        #           or self.options.max_amount > 10000:
        # return [{"error": "too much items for screen or not limited"}]
        numit = 0
        for tw in self.twsource.search_tweets_paginated(KW):
            if self.options.output:
                self.printresult(tw)
            if self.options.store:
                self.storage.store_unique(tw, "id")
            numit += 1
            if self.options.max_amount:
                if numit >= self.options.max_amount:
                    print >> sys.stderr, "*"
                    break
        return None

    cmds = {
        "print_commands": [print_commands, 0, 0],

        "get_tweet_by_id": [get_tweet_id, 1, 0],
        "get_tweets_by_refer": [get_tweets_by_refer, 2, 0],

        "get_conversation_by_id": [get_conversation_by_id, 1, 0],
        "get_conversationtree_by_id":  [get_conversationtree_by_id, 1, 0],
        "get_conversations_from_list":  [get_conversations_from_list, 1, 0],
        "retrieve_conversations_from_list":
            [retrieve_conversations_from_list, 1, 0],
        "generate_collection_from_conversation":
            [generate_collection_from_conversation, 1, 0],

        "get_tweets_from_conversation":  [get_tweets_from_conversation, 1, 0],
        "get_tweets_from_conversation_list":
            [get_tweets_from_conversation, 1, 0],


        "get_tweets_fromURLlist":  [get_tweets_fromURLlist, 1, 0],
        "get_tweets_fromlist":  [get_tweets_fromlist, 1, 0],
        "get_tweets_by_screen": [get_tweets_by_screen, 1, 0],
        "search_tweets_word": [search_tweets_word, 1, 0],

        "search_tweets": [search_tweets, 1, 0],


        # "get_followers": [ get_followers, 1,0 ],
        # "get_deep_followers": [ get_deep_followers, 1,0 ],
        # "delete_deep_followers": [ delete_deep_followers, 1,0 ],
        # "get_tweets": [ get_tweets_screen, 1,0 ],

        # "get_deep_followers_task": [ get_deep_followers_task2, 1,0 ],
        # "run_deep_followers_task": [ run_followers_task, 0,0 ],

        # "dump_followers" : [ dump_followers, -1, 0],
        # "dump_deep_followers" : [dump_deep_followers, 1, 0],
        # "dump_common_followers" : [ dump_common_followers, -1, 0],
        # "dump_multi_common_followers" :
        #           [ dump_multi_common_followers, -1, 0],
        # "dump_following_graph" : [ dump_following_graph, -1, 0],

        # "stats_deep_followers" : [stats_deep_followers, 1, 0],
        # "histo_deep_followers" : [histo_deep_followers, 3, 0],
        # "dump_num_deep_followers" : [dump_num_deep_followers, 1, 0],

        # "get_user_info" : [get_user_info, 1, 0],

    }

    def sanitize_arguments(self):
        """Clean up arguments, checking defaults and restrictions on Arguments
        and validity of all arguments and commands"""
        # self.set_debug_log()
        # self.extraargs = args
        # print getattr(self,"main")

        if len(self.args) < 1:
            cmd = "print_commands"
        else:
            cmd = self.args[0]
        if len(self.args) > 1:
            self.arguments = self.args[1:]
        else:
            self.arguments = None
        cmditem = self.cmds[cmd]

        self.cmdrun = None

        if self.arguments:
            numargs = len(self.arguments)
        else:
            numargs = 0

        if cmditem[1] < 0:
            if numargs < 1:
                print "***", cmditem[1], numargs
                return False
        else:
            if numargs < cmditem[1]:
                return False
            if numargs > cmditem[1] + cmditem[2]:
                return False
        self.cmdrun = cmditem[0]
        return True

    def set_storage(self):
        stparams = {}
        stparams["db"] = self.options.dbname
        if self.options.store or self.options.dynstore:
            stparams["db"] = self.options.dbname
            stparams["collection"] = self.options.dbcollection
            self.storage = storage.StorageDriver.get_driver(
                "mongo")(**stparams)
        else:
            self.storage = storage.StorageDriver.get_driver("noop")()

        if self.options.convs:
            stparams["collection"] = self.options.convs
            self.conv_storage = storage.StorageDriver.get_driver(
                "mongo")(**stparams)
        if self.options.tweets:
            stparams["collection"] = self.options.tweets
            self.tweet_storage = storage.StorageDriver.get_driver(
                "mongo")(**stparams)

    def parse(self, argv=None):
        # Parse Options (Using 2.6 OptParse because of Antonio)
        self.options, self.args = self.parse_args()

        # Clean arguments (checking inconsistencies, etc.)
        retval = self.sanitize_arguments()

        # Return command
        if not retval:
            self.optparse.print_help()
        return self.options

    def set_debug(self):
        loglevels = {
            "CRITICAL": logging.CRITICAL,
            "ERROR": logging.ERROR,
            "WARNING": logging.WARNING,
            "INFO": logging.INFO,
            "DEBUG": logging.DEBUG}

        loglevel = loglevels[self.options.debug]

        # This sets debuglog formatter, level, and handlers
        self.debuglog = logging.getLogger('TWCollect')
        self.debuglog.setLevel(loglevel)
        if self.options.logfile:  # Set Debug Log
            dbgfh = logging.FileHandler(self.options.logfile, mode="w")
        else:
            dbgfh = logging.StreamHandler(sys.stderr)

        dbgformatter = logging.Formatter(
            "%(levelname)s-%(module)s:%(lineno)d -> %(message)s")
        dbgfh.setFormatter(dbgformatter)
        dbgfh.setLevel(loglevel)
        self.debuglog.addHandler(dbgfh)

    def init(self):
        self.set_debug()
        self.set_storage()
        self.storage.initialize()
        self.conv_storage.initialize()
        self.tweet_storage.initialize()

    def run(self):
        if self.cmdrun:
            retdata = self.cmdrun(self, self.arguments)
            # This is a fallback, for those simpler functions
            # that do not print or store data themselves
            if not retdata:
                return
            if self.options.output:
                self.printresult(retdata)
            else:
                if (self.options.showurls):
                    if (self.options.showurls is True):
                        self.printURLs(retdata)
            if self.options.store:
                if not isinstance(retdata, list):
                    retdata = [retdata]
                for retdatum in retdata:
                    print ".",
                    self.storage.store(retdatum)  # _unique(retdatum,"_id")

    def cleanup(self):
        self.storage.cleanup()
        pass

if __name__ == '__main__':
    twc = TwCollect()
    opts = twc.parse(sys.argv)
    twc.init()
    twc.run()
    twc.cleanup()
