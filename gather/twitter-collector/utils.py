
import logging


# loglev = logging.ERROR
logger = logging.getLogger('TWCollect')
# Configure logger to write to file/stdout etc, it's level etc
# logger.setLevel(loglev)
# consoleh = logging.StreamHandler()
# consoleh.setLevel(loglev)
# logger.addHandler(consoleh)

# fileh = logging.FileHandler("twcollect.log", mode="w")
# fileh.setLevel(loglev)
# logger.addHandler(fileh)


def logfunction(level):
    def _decorator(fn):
        def _decorated(*arg, **kwargs):
            logger.log(level, "calling '%s'(%r,%r)", fn.func_name, arg, kwargs)
            ret = fn(*arg, **kwargs)
            logger.log(level, "called '%s'(%r,%r) got return value: %r",
                       fn.func_name, arg, kwargs, ret)
            return ret
        return _decorated
    return _decorator

#
# With different loggre


def logfunction2(logger, level):
    def _decorator(fn):
        def _decorated(*arg, **kwargs):
            logger.log(level, "calling '%s'(%r,%r)", fn.func_name, arg, kwargs)
            ret = fn(*arg, **kwargs)
            logger.log(level, "called '%s'(%r,%r) got return value: %r",
                       fn.func_name, arg, kwargs, ret)
            return ret
        return _decorated
    return _decorator
