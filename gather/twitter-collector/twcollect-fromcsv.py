#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''
Originally Created on 01/10/2012
Rebootstrapped 01/22/2015

@author: carlesm
'''


import optparse
import sys
import time
import math
import json

import  dcoll

class TwCollect():

    def __init__(self):
        pass

    def parse_args(self):
        """Will parse options on cmd line"""

        p = optparse.OptionParser()

        grpauth = optparse.OptionGroup(p, "Auth Options",
                                       "Oauth/Auth/Login options.")
        grpauth.add_option('-u', '--oauth', action='store', dest='oauthf',
                           help='Oauth File')
        p.add_option_group(grpauth)



        
        grpout = optparse.OptionGroup(p, "Output Options",
                    "Output control options.")
        
        grpout.add_option('-r',  '--readable',  action='store_true',  dest='readable',
                    help='Readable text output file (JSON/XML)')

        p.add_option_group(grpout)


        # DEBUG OPTIONS
        grpdbg = optparse.OptionGroup(
            p, "Debug Options", "Debug control options.")

        grpdbg.add_option('-l', '--log', action='store', dest='logfile',
                          help='Log to file')

        grpdbg.add_option('-d', '--debug', action='store', type='choice',
                          choices=[
                              "CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
                          help='''Logging level:          '''
                               '''CRITICAL, ERROR, WARNING, INFO, DEBUG''')
        # p.add_option_group(grpdbg)

        p.set_defaults(
            logfile=None,
            debug="DEBUG",
            readable = False,
            oauthf="twcollect.oauth",
        )
        options, args = p.parse_args()
        self.optparse = p
        return options, args


    def init_twitter(self):
        self.twsource = dcoll.twit()
        
        # twsource.setAuth() <- set auth data
        # twsource.fetchData() <- get data
        # while twsource.cursor()
        #    db.store()
        
        
        #    CARLESM
        #    APP_NAME = 'twtour'
        #    CONSUMER_KEY = 'SzhFl1tIaeuwgfgugSx2Pw'
        #    CONSUMER_SECRET = '5FRDk2z3rvIErI3uHARQMnt1N7FJMmgZ2z7ASRMjM'

        # CCIA2011
        APP_NAME = 'twtour01'
        CONSUMER_KEY = 'yIDPUaYIxSidu0oTVmN86A'
        CONSUMER_SECRET = 'X8AtcollGGergCDjGXkvYkalPUuczGlJCOh6dXrF7w'
        self.twsource.set_auth(APP_NAME, CONSUMER_KEY, 
                               CONSUMER_SECRET, self.options.oauthf)
        self.twsource.auth()

    def get_tweet_id(self, args):
        self.init_twitter()
        a = self.twsource.get_single_tweet(args[0])
        return a

    def get_tweets_fromlist(self, args):
        self.init_twitter()
        response = []
        f = open( args[0], "r" )
        for line in f:
          toks = line.split()
          response.append(self.twsource.get_single_tweet(toks[0]))          
        f.close()
        return response

    def get_tweets_by_refer(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "to:%s" % (args[0])
        KW['since_id'] = args[1]
        search_result = self.twsource.search_tweets(KW)
        tweets = search_result['statuses']
        # for tweet in tweets:
        #     print tweet
        return tweets

    def get_conversation_by_id(self, args):
        self.init_twitter()
        orig_tweet = self.twsource.get_single_tweet(args[0])
        refered_to = orig_tweet['user']['screen_name']
        tweet_id = orig_tweet['id']
        response = [orig_tweet]
        for i in self.get_tweets_by_refer([refered_to, tweet_id]):
            if i['in_reply_to_status_id'] == tweet_id:
                response.append(i)
        #for r in response:
        #    print json.dumps(r)
        return response


    def print_commands(self, args):
        print "Available commands:"
        for i in self.cmds:
            print "\t", i
        self.optparse.print_help()


    def get_tweets_by_screen(self, args):
        self.init_twitter()
        j=0
        tws=[]
        for i in self.twsource.get_next_timeline(args[0], rts=True):            
                tws.append(i)
        return tws
    
    def search_tweets_word(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "%s" % (args[0])
        #KW['since_id'] = args[1]
        search_result = self.twsource.search_tweets_paginated(KW)
        
        for i in search_result:
            print i
        return []
        # tweets = search_result['statuses']
        # # for tweet in tweets:
        # #     print tweet
        # return tweets

    cmds = {
        "print_commands": [print_commands, 0, 0],
        "get_tweet_by_id": [get_tweet_id, 1, 0],
        "get_tweets_by_refer": [get_tweets_by_refer, 2, 0],
        "get_conversation_by_id": [get_conversation_by_id, 1, 0],
        "get_tweets_by_screen": [ get_tweets_by_screen, 1,0 ],
        "search_tweets_word": [ search_tweets_word, 1,0 ], 
        "get_tweets_by_id": [ get_tweets_fromlist, 1, 0]


        # "get_followers": [ get_followers, 1,0 ],
        # "get_deep_followers": [ get_deep_followers, 1,0 ],
        # "delete_deep_followers": [ delete_deep_followers, 1,0 ],
        # "get_tweets": [ get_tweets_screen, 1,0 ],

        # "get_deep_followers_task": [ get_deep_followers_task2, 1,0 ],
        # "run_deep_followers_task": [ run_followers_task, 0,0 ],

        # "dump_followers" : [ dump_followers, -1, 0],
        # "dump_deep_followers" : [dump_deep_followers, 1, 0],
        # "dump_common_followers" : [ dump_common_followers, -1, 0],
        # "dump_multi_common_followers" :
        #           [ dump_multi_common_followers, -1, 0],
        # "dump_following_graph" : [ dump_following_graph, -1, 0],

        # "stats_deep_followers" : [stats_deep_followers, 1, 0],
        # "histo_deep_followers" : [histo_deep_followers, 3, 0],
        # "dump_num_deep_followers" : [dump_num_deep_followers, 1, 0],

        # "get_user_info" : [get_user_info, 1, 0],

    }

    def sanitize_arguments(self):
        """Clean up arguments, checking defaults and restrictions on Arguments
        and validity of all arguments and commands"""
        # self.set_debug_log()
        # self.extraargs = args
        # print getattr(self,"main")


        if len(self.args) < 1:
            cmd = "print_commands"
        else:
            cmd = self.args[0]
        if len(self.args) > 1:
            self.arguments = self.args[1:]
        else:
            self.arguments = None
        cmditem = self.cmds[cmd]
        
        self.cmdrun=None

        if self.arguments:
            numargs = len(self.arguments)
        else:
            numargs = 0

        if cmditem[1] < 0:
            if numargs < 1:
                print "***", cmditem[1], numargs
                return False
        else:
            if numargs < cmditem[1]:
                return False
            if numargs > cmditem[1] + cmditem[2]:
                return False
        self.cmdrun = cmditem[0]
        return True

    def main(self, argv=None):
        # Process Arguments
        self.options, self.args = self.parse_args()
        # print self.options
        # print self.args
        # -> Set Callbacks?
        retval = self.sanitize_arguments()
        # -> Call command
        if not retval:
            self.optparse.print_help()
            return self.options
        return self.options

    def run(self):
        if self.cmdrun:
            retdata=self.cmdrun(self, self.arguments)
        if not isinstance(retdata, list):
            retdata = [retdata]
            
        for retdatum in retdata:
            if self.options.readable:
                print json.dumps(retdatum, indent=4)
            else:
                print json.dumps(retdatum)




if __name__ == '__main__':
    twc = TwCollect()
    opts = twc.main(sys.argv)
    # print twc.options
    twc.run()
