#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''
Originally Created on 01/10/2012
Rebootstrapped 01/22/2015

To process dynamically generated web pages,
we should change to something like selenium:

http://stackoverflow.com/questions/8454694/python-retrieve-dynamic-content-using-urllib2

@author: ramon
'''

import optparse
import sys
import json
#import dcoll
import storage
import re
import time
import urllib2

import urlparse

# from BeautifulSoup import BeautifulSoup
# from contextlib import closing
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support import ui

from selenium.webdriver.common.keys import Keys


class TwconcDownloader(object):
    def __init__(self,login,password):
        self.logged = False
        self.browseropened = False
        self.login = login
        self.password = password

    def openBrowser(self):
        self.browser = Firefox()
        self.browseropened = True
        self.wait = ui.WebDriverWait(self.browser, 30)
        self.logged = False

    def closeBrowser(self):
        self.browser.close()
        self.browseropened = False

    def showConversation(self,convurl):
        self.browser.get(convurl)

    def loginTwuser( self ):
        if (self.logged == False):
          # time.sleep(4)
          loginlink = self.browser.find_element_by_id("signin-link")
          if (loginlink != None):
             try:
               loginlink.click()
               loginitem = self.browser.find_element_by_xpath("//input[@name='session[username_or_email]']")
               passworditem = self.browser.find_element_by_xpath("//input[@name='session[password]']")
               if ( (loginitem != None) & (passworditem != None) ):
                  loginitem.send_keys(self.login)
                  passworditem.send_keys(self.password)
                  passworditem.send_keys(Keys.RETURN)
             except:
               pass
             self.logged = True

    def scrollDown(self):
        bottom = False
        height =  int(self.browser.execute_script("return document.body.scrollHeight" ))
        quarter = str(height/4)
        while (not bottom):
           # prevscrollpos = self.browser.execute_script("return document.activeElement.scrollTop")
           prevscrollpos = self.browser.execute_script("return window.pageYOffset")
           self.browser.execute_script("window.scrollBy(0,"+quarter+")")
           # newscrollpos = self.browser.execute_script("return document.activeElement.scrollTop")
           newscrollpos = self.browser.execute_script("return window.pageYOffset")
           print prevscrollpos, " - ", newscrollpos
           bottom = newscrollpos <= prevscrollpos

    def dowloadsubconv(self,twurl,maxtweets):

          self.browser.get(twurl)
          time.sleep(4)
          tweets1 = [ el.get_attribute('data-permalink-path') for el in self.browser.find_elements_by_xpath( "//*[@data-permalink-path]" ) ]
          size1 = len(tweets1)
          if (size1 < 1):
              return
          for tw2url in tweets1:
              if (not tw2url.startswith("https://twitter.com/")):
                  tw2url = "https://twitter.com"+tw2url
              if (len(self.convtweetids) < maxtweets):
                tw2id = tw2url.split('/')[-1]
                if (tw2id not in self.convtweetids):
                  self.convtweetids.append(tw2id)
                  print "new tweet: ", tw2url
                  self.dowloadsubconv(tw2url,maxtweets)

    def dowloadRecConversation(self,convurl,maxtweets):
        self.convtweetids = []
        twurl = convurl.strip()
        if (self.browseropened == False):
             self.openBrowser()
        if (twurl.startswith("https://twitter.com/")):
           self.convtweetids.append(twurl.split('/')[-1])
           print ">>>> new ROOT tweet: ", twurl
           self.dowloadsubconv(twurl,maxtweets)

        return self.convtweetids

    def downloadConversation(self,convurl):
        twurl = convurl.strip()
        if (self.browseropened == False):
            self.openBrowser()
        if (twurl.startswith("https://twitter.com/")):
            self.browser.get(twurl)
            time.sleep(6)
#            if (self.logged == False):
#                self.loginTwuser()
#                time.sleep(3)
            tweets1 = self.browser.find_elements_by_xpath( "//*[@data-permalink-path]" )
            size1 = len(tweets1)
            size2 = size1+1
            self.scrollDown()
# element.scrollHeight - element.scrollTop === element.clientHeight
            print "--> ", self.browser.execute_script("return   window.scrollY")
            # print  "Page y offset : ", self.browser.execute_script("return document.activeElement.scrollHeight")
            while (size1 != size2):
                     # self.browser.execute_script("window.scrollTo(0, Math.max( document.body.scrollHeight, document.body.offsetHeight, document.body.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight, document.documentElement.clientHeight ));")
                     self.scrollDown()
                     print "--> ", self.browser.execute_script("return   window.scrollY")
                     time.sleep(10)
                     morelinks = self.browser.find_elements_by_class_name('show-more-link')
                     if (morelinks is not None):
                       for mlink in morelinks:
                          print mlink.get_attribute("href")
                          mlink.click()
                          time.sleep(5)
                          # self.browser.execute_script("window.scrollTo(0, Math.max( document.body.scrollHeight, document.body.offsetHeight, document.body.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight, document.documentElement.clientHeight ));")
                          self.scrollDown()
                          print "--> ", self.browser.execute_script("return   window.scrollY")
                          time.sleep(5)
                     size1=size2
                     # time.sleep(10)
                     self.scrollDown()
                     print "--> ", self.browser.execute_script("return   window.scrollY")
                     time.sleep(5)
                     tweets2 = self.browser.find_elements_by_xpath( "//*[@data-permalink-path]" )
                     size2 = len(tweets2)
                     w = raw_input('cont?')
            return [ (str(tw.get_attribute('data-permalink-path')).split('/'))[-1]   for tw in tweets2]
        else:
            return []


    def downloadSetofConversations(self,convlistfile,minsize,maxtweets):

      numconv = 1
      self.totalSetOfTweetids = []
      f = open(convlistfile,"r")
      for l in f:
        twcurl = l.strip()
        twurltoks = twcurl.split('/')
        if (twurltoks[-1] in self.totalSetOfTweetids):
            print "Conv : ", twcurl, " already dowloaded "
            numconv = numconv + 1
            continue

        listoftweetids = self.dowloadRecConversation(twcurl,maxtweets)
        self.totalSetOfTweetids = self.totalSetOfTweetids + listoftweetids
        size = len(listoftweetids)
        if (size >= minsize):
           ofile = open( convlistfile+"_c"+str(numconv)+twurltoks[-3]+"_"+twurltoks[-1]+".convids", "w")
           for it in listoftweetids:
             #print it
             #print it.get_attribute('data-permalink-path')
             ofile.write( it + "\n" )
           ofile.close()
        numconv = numconv+1
      f.close()




if __name__ == '__main__':


    login = sys.argv[2]
    password = sys.argv[3]
    TwDown = TwconcDownloader(login , password )
    if (sys.argv[1].startswith("https://twitter.com/")):
       convurl = sys.argv[1]
       tweetids = TwDown.dowloadRecConversation(convurl,100)
       if (tweetids != []):
           ofile = open( sys.argv[4], "w" )
           for twid in tweetids:
             ofile.write(str(twid)+'\n')
           ofile.close()
       TwDown.closeBrowser()
    else:
       input_file = sys.argv[1]
       minsize = int(sys.argv[4])
       TwDown.downloadSetofConversations( input_file, minsize, 100 )
       TwDown.closeBrowser()
