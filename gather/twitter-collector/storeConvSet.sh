#!/bin/bash

prefix=$1
for file in *.convids
do
  rex=$prefix+'\.URLs\_(.*)\.convids'
  if [[ $file =~ $rex ]] ;
  then
    echo 'python twcollect.py -s -a '$prefix' -c '${BASH_REMATCH[1]}' get_tweets_fromlist '$file
    python $2/twcollect.py -s -a $prefix -c ${BASH_REMATCH[1]} get_tweets_fromlist $file
  fi
done
