#!/bin/bash

prefix=$1
for url in `cat $2`
do
  usern=`echo $url | cut -f 4 -d "/"`
  idr=`echo $url | cut -f 6 -d "/"`
  echo 'python twcollect.py -s -a '$prefix' -c '$usern'-'$idr' get_conversation_by_id '$idr
  python twcollect.py -s -a $prefix -c $usern'-'$idr get_conversation_by_id $idr

#    python $2/twcollect.py -s -a $prefix -c ${BASH_REMATCH[1]} get_tweets_fromlist $file
#  fi
done
