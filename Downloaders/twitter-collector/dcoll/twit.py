'''
Created on 01/10/2012

@author: carlesm
'''

import sys
import twitter
from twitter.oauth import write_token_file, read_token_file
from twitter.oauth_dance import oauth_dance
import time
from urllib2 import URLError
import json
import urlparse
import logging

from utils import *


class twit(object):

    '''
    Twitter data source
    '''

    def __init__(self):
        '''
        Creates data source
        '''
        # Empty twitter object (will create later)
        self.t = None
        pass

    def set_auth(self, app_name, consumer_key, consumer_secret,
                 oauth_file=None):
        """
        Sets authtentication parameters
        """
        self.app_name = app_name
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.oauth_file = oauth_file

    def auth(self):
        if not self.oauth_file:
            self.oauth_file = "twitter.oauth"

        # try:
        (self.access_token, self.access_token_secret) = read_token_file(
            "Downloaders/twitter-collector/twcollect.oauth")
        auth = twitter.oauth.OAuth(self.access_token,
                                   self.access_token_secret,
                                   self.consumer_key, self.consumer_secret)
        self.t = twitter.Twitter(domain='api.twitter.com',
                                 api_version='1.1',
                                 auth=auth)

    @logfunction(logging.DEBUG)
    def _make_twitter_request(self, t, api_call, max_errors=3, *args, **kwArgs):
        # Handling common HTTPErrors:
        # Return an updated value for wait_period if the problem is a 503 error.
        # Block until the rate limit is reset if a rate limiting issue

        def handle_http_error(e, t, api_call, wait_period=2, sleep_when_rate_limited=True):
            print >> sys.stderr, 'Problem in api call:', api_call
            if wait_period > 3600:  # Seconds
                print >> sys.stderr, 'Too many retries.'
                raise e
            if e.e.code == 401:
                print >> sys.stderr, 'Encountered 401 Error (Not Authorized)'
                return None
            elif e.e.code in (502, 503):
                print >> sys.stderr, '%i Error. Retry in %i seconds' % \
                    (e.e.code, wait_period)
                time.sleep(wait_period)
                wait_period *= 1.5
                return wait_period
            elif e.e.code == 429:
                print >> sys.stderr, 'Encountered 429 Error (Rate Limit Exceeded)'
                if sleep_when_rate_limited:
                    print >> sys.stderr, "Sleeping for 15 minutes, and then I'll try again...ZzZ..."
                    time.sleep(60 * 15 + 5)
                    print >> sys.stderr, '...ZzZ...Awake now and trying again.'
                    return 2
                else:
                    raise e  # To the application
            # elif t.application.rate_limit_status()['remaining_hits'] == 0:
            #     status = t.application.rate_limit_status()
            #     now = time.time()  # UTC
            #     when_rate_limit_resets = status['reset_time_in_seconds']  # UTC
            #     sleep_time = when_rate_limit_resets - now
            #     print >> sys.stderr, 'Rate limit: sleeping for %i s.' % \
            #         (sleep_time, )
            #     time.sleep(sleep_time)
            #     return 2
            else:
                raise e

        wait_period = 2
        error_count = 0
        while True:
            try:
                return api_call(*args, **kwArgs)
            except twitter.api.TwitterHTTPError, e:
                error_count = 0
                wait_period = handle_http_error(
                    e, t, api_call, wait_period, sleep_when_rate_limited=True)
                if wait_period is None:
                    return
            except URLError, e:
                error_count += 1
                print >> sys.stderr, "URLError encountered. Continuing."
                if error_count > max_errors:
                    print >> sys.stderr, "Too many errors... quitting cowardly."
                    raise

        pass

    def get_next_timeline(self, screen_name, rts=True):
        '''
        Iterator over <screen_name> timeline
        '''

        KW = {  # For the Twitter API call
            'count': 200,
            'skip_users': 'true',
            'include_entities': 'true',
            'since_id': 1,
        }
        if rts:
            KW['include_rts'] = 'true'
        else:
            KW['include_rts'] = 'false'

        KW['screen_name'] = screen_name
        page_num = 1
        while page_num <= 16:
            KW['page'] = page_num
            api_call = getattr(self.t.statuses, 'user_timeline')
            tweets = self._make_twitter_request(self.t, api_call, **KW)
            for i in tweets:
                yield i
            page_num += 1

    def get_follower_ids(self, screen_name):
        '''
        Get all followers
        '''

        KW = {
            'screen_name': screen_name
        }
        cursor = -1
        while cursor != 0:
            KW['cursor'] = cursor
            api_call = getattr(self.t.followers, 'ids')
            resposta = self._make_twitter_request(self.t, api_call, **KW)
            cursor = resposta['next_cursor']
            # for i in tweets:
            #    yield i
            yield resposta

    def get_follower_ids_by_id(self, id):
        '''
        Get all followers
        '''

        KW = {
            'user_id': id
        }
        cursor = -1
        while cursor != 0:
            KW['cursor'] = cursor
            api_call = getattr(self.t.followers, 'ids')
            resposta = self._make_twitter_request(self.t, api_call, **KW)
            if resposta:
                # pass
                cursor = resposta['next_cursor']
            else:
                cursor = 0
                # pass
            # for i in tweets:
            #    yield i
            yield resposta

    def get_lists_useris(self, screen_name):
        KW = {}
        KW['screen_name'] = screen_name
        api_call = getattr(self.t.lists, 'memberships')
        resposta = self._make_twitter_request(self.t, api_call, **KW)
        return resposta

    def get_user(self, screen_name):
        KW = {}
        KW['screen_name'] = screen_name
        api_call = getattr(self.t.users, 'show')
        resposta = self._make_twitter_request(self.t, api_call, **KW)
        return resposta

    def users_lookup(self, params, by_screen=True):
        print ",".join(params)
        return
        # KW = {}
        # if by_screen:
        #     KW['screen_name'] = ",".join(params)
        # else:
        #     KW['user_id'] = ",".join(params)

        # api_call = getattr(self.t.users, 'lookup')
        # resposta = self._make_twitter_request(self.t, api_call, **KW)
        # return resposta

    def get_single_tweet(self, tweet_id):
        KW = {}
        KW['id'] = tweet_id
        api_call = getattr(self.t.statuses, 'show')
        resposta = self._make_twitter_request(self.t, api_call, **KW)
        # print resposta
        return resposta

    def search_tweets(self, KW):
        KW['count'] = "100"  # Maximum 100 tweets (API)
        KW['result_type'] = "json"
        KW['include_entities'] = "true"
        api_call = getattr(self.t.search, 'tweets')
        resposta = self._make_twitter_request(self.t, api_call, **KW)
        # print resposta
        return resposta

    def search_tweets_paginated(self, KW):
        KW['result_type'] = "json"
        KW['include_entities'] = "true"
        KW['count'] = "100"
        #KW['f'] = "realtime"
        KW['result_type'] = "mixed"
        api_call = getattr(self.t.search, 'tweets')
        max_id = '-1'
        while max_id != '0':
            resposta = self._make_twitter_request(self.t, api_call, **KW)
            if 'next_results' in resposta['search_metadata']:
                max_id = dict(
                    urlparse.parse_qsl(
                        resposta['search_metadata']['next_results'][1:])
                    )['max_id']
            else:
                max_id = '0'
            KW['max_id'] = max_id
            data_response = resposta['statuses']
            for tweet in data_response:
                yield tweet



    def clean_twit(self, tweet):
        return tweet
