"""
    Data collection classes


    Includes:
        twitter

"""
from dcoll.twit import *

import sys
if sys.version_info[:2] < (2, 6):
    m = "Python version 2.6 or later is required to work (%d.%d detected)."
    raise ImportError(m % sys.version_info[:2])
del sys

# Release

__author__ = 'Carles Mateu <carlesm@carlesm.com>'
__license__ = "License :: OSI Approved :: GPL License"
__date__ = "Oct 2012"
__version__ = "0.0.2"

# these packages work with Python >= 2.6


__all__ = ["twit"]
