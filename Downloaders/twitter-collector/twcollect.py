#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :
'''
Originally Created on 01/10/2012
Rebootstrapped 01/22/2015

@author: carlesm
'''


import optparse
import sys
import json
import dcoll

import requests
import urlparse
import logging

from utils import *
from tqdm import tqdm

import re


try:
    from html import unescape  # python 3.4+
except ImportError:
    try:
        from html.parser import HTMLParser  # python 3.x (<3.4)
        unescape = HTMLParser().unescape
    except ImportError:
        from HTMLParser import HTMLParser  # python 2.x
        unescape = HTMLParser().unescape


import urllib3
import urllib3.contrib.pyopenssl
urllib3.disable_warnings()
urllib3.contrib.pyopenssl.inject_into_urllib3()
logging.captureWarnings(True)


class TwCollect(object):

    def __init__(self):
        pass

    def parse_args(self):
        """Will parse options on cmd line"""

        p = optparse.OptionParser()
        p.set_defaults(
            logfile=None,
            debug="CRITICAL",
            readable=False,
            oauthf="twcollect.oauth",
            max_amount=None,
            dbname="twcollect",
            dbcollection="tweets",
            store=False,
            dynstore=False,
            output=True,
            tweetids=False,
            convs="conversations",
            tweets="tweets",
        )

        options, args = p.parse_args()
        self.optparse = p
        return options, args

    def init_twitter(self):
        self.twsource = dcoll.twit()

        APP_NAME = #Insert AppName
        CONSUMER_KEY = #Insert ConsumerKey
        CONSUMER_SECRET = #Insert Secret
        self.twsource.set_auth(APP_NAME, CONSUMER_KEY,
                               CONSUMER_SECRET, self.options.oauthf)
        self.twsource.auth()

    def get_tweet_id(self, args):
        self.init_twitter()
        return self.twsource.get_single_tweet(args[0])

    def get_tweets_by_refer(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "to:%s" % (args[0])
        KW['since_id'] = args[1]
        search_result = self.twsource.search_tweets(KW)
        tweets = search_result['statuses']
        return tweets

    # get all tweets where the screen name @args[0] appears
    def get_tweets_by_refer2(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "@%s" % (args[0])
        KW['since_id'] = args[1]
        return self.twsource.search_tweets_paginated(KW)
        # tweets = search_result['statuses']
        # return tweets

    def get_conversation_by_id_removed_code(self, args):
        self.init_twitter()
        orig_tweet = self.twsource.get_single_tweet(args[0])
        refered_to = orig_tweet['user']['screen_name']
        tweet_id = orig_tweet['id']
        response = [orig_tweet]
        for i in self.get_tweets_by_refer([refered_to, tweet_id]):
            if i['in_reply_to_status_id'] == tweet_id:
                response.append(i)
        # for r in response:
        #    print json.dumps(r)
        return response

    def _clean_page(self, page):
        newtext = page  # .encode('utf-8')
        newtext = newtext.replace('\\n', '\n')
        newtext = unescape(newtext)
        newtext = newtext.replace('\\u003c', '<')
        newtext = newtext.replace('\\u003e', '>')
        newtext = newtext.replace('\\/', '/')
        newtext = newtext.replace('\\\"', '\"')
        return newtext

    def _get_conversation_ids(self, username, twtid, tweets):
        # repeat:
        #   Get html
        #   Get tweetids from HTML/HTML_FRAGMENT_IN_JSON
        #   recurse to tweetids
        #   if ! data-min-position in HTML
        #       break
        repeat = True
        url = "https://twitter.com/%s/status/%s" % (str(username), str(twtid))
        reg = re.compile(r'<a href=\"(/[^/]*/status/[0-9]*)\" class=\"tweet-timestamp js-permalink js-nav js-tooltip\"')
        rega = re.compile(r'data-min-position=\"([^\"]*)\"')
        regb = re.compile(r'\"min_position\":\"([^\"]*)\"')
        convbase = "https://twitter.com/i/"
        convargs = "?include_available_features=1&include_entities=1&"\
                   "max_position="
        nextt = ""
        while repeat:
            repeat = False
            oldnext = nextt
            page = requests.get(url).text
            page = self._clean_page(page)
            items = reg.findall(page)
            for it in items:
                s1, username2, s2, twtid2 = it.split('/')
                if twtid2 not in tweets:
                    tweets.append(twtid2)
                    self._get_conversation_ids(username2, twtid2, tweets)
            m1 = rega.search(page)
            if m1:
                nextt = m1.group(1)
            else:
                m2 = regb.search(page)
                if m2:
                    nextt = m2.group(1)
            if nextt and nextt is not oldnext:
                url = "%s%s/conversation/%s%s%s" % (convbase, str(username),
                                                    str(twtid), convargs,
                                                    nextt)
                repeat = True

    def _get_conversation_tree_by_id(self, tweetid):
        try:
            orig_tweet = self.twsource.get_single_tweet(tweetid)
        except Exception as e:
            print e, tweetid
            return []
        username = orig_tweet['user']['screen_name']
        tweetids = []
        self._get_conversation_ids(username, tweetid, tweetids)
        return tweetids

    def get_conversation_by_id(self, args):
        self.init_twitter()
        tweetids = self._get_conversation_tree_by_id(args)
        response = []
        for twtid in tweetids:
            resp = self.twsource.get_single_tweet(twtid)
            if resp:
                response.append(resp)
        return response

    def get_conversations_from_list(self, args):
        # for i in list:
        #   if i is URL:
        #       extract id
        #   get_conversation_by_id(i)
        #  if store:
        #    store_results
        # return results ?
        reg = re.compile(r'https://twitter.com/.+/status/([0-9]*)')
        responsed = {}
        response = []
        f = open(args[0], "r")
        for line in tqdm(f):
            if "http" in line:
                idtweet = reg.findall(line)[0]
            tweets = self._get_conversation_tree_by_id(idtweet)
            for t in tweets:
                responsed[t] = responsed.get(t, 0) + 1
                if t not in response:
                    response.append(t)
        f.close()
        total = 0.0
        maximum = len(responsed)
        maxim05 = maximum * 0.5
        for k in responsed:
            if k >= maxim05:
                print responsed[k], k
            total += float(responsed[k])
        print maximum, total, total/maximum
        return response

    def retrieve_conversations_from_list(self, args):
        """
        Returns conversations (for storage) from a list of IDs or URLs

        Format:
        [
            { conversation_id,
              root_id,
              teets: [ids, ...]
              length
            }
        ]
        """
        # for i in list:
        #   if i is URL:
        #       extract id
        #   get_conversation_by_id(i)
        #  if store:
        #    store_results
        # return results ?
        self.init_twitter()
        reg = re.compile(r'https://twitter.com/.+/status/([0-9]*)')
        response = []
        f = open(args[0], "r")
        for line in tqdm(f):
            if "http" in line:
                idtweet = reg.findall(line)[0]
            else:
                idtweet = line
            tweets = self._get_conversation_tree_by_id(idtweet)
            if tweets and len(tweets) > 0:
                conversation = {}
                conversation["root_id"] = tweets[0]
                conversation["length"] = len(tweets)
                conversation["tweets"] = tweets
                conversation["conversation_id"] = idtweet
                response.append(conversation)
        f.close()
        return response

    def _get_tweets_from_conversation(self, args):
        conv = self.conv_storage.find_one("conversation_id", args[0])
        if conv:
            tweets = conv["tweets"]
            response = []
            for t in tweets:
                resp = self.twsource.get_single_tweet(t)
                # TODO: add dyn-store support
                if resp:
                    response.append(resp)
        else:
            response = []
        return response

    def get_tweets_from_conversation(self, args):
        self.init_twitter()
        return self._get_tweets_from_conversation(args)

    def get_tweets_from_conversation_list(self, args):
        # for i in list:
        #   if i is URL:
        #       extract id
        #   get_tweets_from_conversation(i)
        # return results ?
        self.init_twitter()
        reg = re.compile(r'https://twitter.com/.+/status/([0-9]*)')
        response = []
        f = open(args[0], "r")
        for line in tqdm(f):
            if "http" in line:
                idtweet = reg.findall(line)[0]
            else:
                idtweet = args[0]
            tweets = self.get_tweets_from_conversation([idtweet])
            for t in tweets:
                # TODO: add dyn-store support
                if t not in response:
                    response.append(t)
        f.close()
        return response

    def generate_collection_from_conversation(self, args):
        #
        # Select conversation
        # for t in tweets:
        #   add result <- tweets
        # return result
        conv = self.conv_storage.find_one("conversation_id", args[0])
        response = []
        if conv:
            tweets = conv["tweets"]
            for t in tweets:
                tweet = self.tweet_storage.find_one("id_str", t)
                if tweet:
                    # print tweet["id"], tweet["user"]["name"]
                    response.append(tweet)
        return response

    def get_subtree_by_tweet(self, tw):
        # store sub-root
        orig_tweet = tw
        refered_to = orig_tweet['user']['screen_name']
        tweet_id = orig_tweet['id']
        subtree = [orig_tweet]
        # print "getting subtree for ", tweet_id, " screen name", refered_to
        # search child nodes
        for i in self.get_tweets_by_refer2([refered_to, tweet_id]):
            # print "child tweet id", i['id'], "in reply to"\
            #       ": ", i['in_reply_to_status_id']
            if i['in_reply_to_status_id'] == tweet_id:
                subtree += self.get_subtree_by_tweet(i)

        return subtree

        # recursively get a conversation tree
    def get_conversationtree_by_id(self, args):
        self.init_twitter()
        # get root tweet
        orig_tweet = self.twsource.get_single_tweet(args[0])
        refered_to = orig_tweet['user']['screen_name']
        tweet_id = orig_tweet['id']
        # print "getting tree for ", args[0], " screen name", refered_to
        tree = [orig_tweet]
        # search child nodes
        for i in self.get_tweets_by_refer2([refered_to, tweet_id]):
            # print "child tweet id", i['id'], "in reply to: ", \
            # i['in_reply_to_status_id']
            if i['in_reply_to_status_id'] == tweet_id:
                tree += self.get_subtree_by_tweet(i)

        return tree

    def get_tweets_fromlist(self, args):
        self.init_twitter()
        response = []
        f = open(args[0], "r")
        for line in f:
            toks = line.split()
            try:
                resp = self.twsource.get_single_tweet(toks[0])
                if resp is not None:
                    response.append(resp)
            except:
                pass
        f.close()
        return response

    def get_tweets_fromURLlist(self, args):
        self.init_twitter()
        response = []
        f = open(args[0], "r")
        for line in f:
            # print line
            urltweet = urlparse.urlparse(line)
            if (urltweet.path != ''):
                toks = urltweet.path.split('/')
                # print toks
                # print toks[-1]
                # size = len(toks)
            response.append(self.twsource.get_single_tweet(toks[-1].strip()))
        f.close()
        return response

    def print_commands(self, args):
        print "Available commands:"
        for i in self.cmds:
            print "\t", i
        self.optparse.print_help()

    def get_tweets_by_screen(self, args):
        self.init_twitter()
        tws = []
        twitcnt = 1
        for i in self.twsource.get_next_timeline(args[0], rts=True):
            tws.append(i)
            twitcnt += 1
            if self.options.max_amount and twitcnt > self.options.max_amount:
                break
        return tws

    def search_tweets_word_list(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "%s" % (args[0])
        # KW['since_id'] = args[1]
        tweets = list(self.twsource.search_tweets_paginated(KW))
        return tweets

    def printresult(self, *args):
        for arg in args:
            if not isinstance(arg, list):
                arg2 = [arg]
            else:
                arg2 = arg
            storeFile = open("Storage/Jsons/" + requestedID + ".json", "w")
            finalChar = ""
            for itm in arg2:
                storeFile.write(finalChar + json.dumps(itm))
                finalChar = "\n"
            # print "Download finished, stored on Storage/Jsons/" + requestedID + ".json"
            storeFile.close()

    def printURLs(self, *args):
        for arg in args:
            if not isinstance(arg, list):
                arg2 = [arg]
            else:
                arg2 = arg
            for itm in arg2:
                if (self.options.showurls):
                    if (self.options.showurls is True):
                        print "https://twitter.com/" + \
                            itm['user']['screen_name'] + \
                            "/status/"+str(itm['id'])

    def search_tweets_word(self, args):
        self.init_twitter()
        KW = {}
        KW['q'] = "%s" % (args[0])
        # if not self.options.store and self.options.output:
        # if Storing there's no maximum
        # TODO: Must add option to screen out (the limited one)
        # if self.options.screenout
        #     if not self.options.max_amount
        #           or self.options.max_amount > 10000:
        # return [{"error": "too much items for screen or not limited"}]
        numit = 0
        for tw in self.twsource.search_tweets_paginated(KW):
            if self.options.store:
                self.storage.store_unique(tw, "id")
                # self.storage.store(tw) #, "id")
            if self.options.output:
                self.printresult(tw)
            else:
                if (self.options.showurls):
                    if (self.options.showurls is True):
                        print "https://twitter.com/" + \
                              tw['user']['screen_name'] + \
                              "/status/"+str(tw['id'])

            numit += 1
            if self.options.max_amount:
                if numit >= self.options.max_amount:
                    break
        print >> sys.stderr, "TOT: ", numit
        return None

    def search_tweets(self, args):
        self.init_twitter()
        # KW = {}
        # Args will be a dict JSON style (as of now)
        KW = json.loads(args[0])
        # print KW
        # KW['q'] = "%s" % (args[0])
        # if not self.options.store and self.options.output:
        # if Storing there's no maximum
        # TODO: Must add option to screen out (the limited one)
        # if self.options.screenout
        #     if not self.options.max_amount
        #           or self.options.max_amount > 10000:
        # return [{"error": "too much items for screen or not limited"}]
        numit = 0
        for tw in self.twsource.search_tweets_paginated(KW):
            if self.options.output:
                self.printresult(tw)
            if self.options.store:
                self.storage.store_unique(tw, "id")
            numit += 1
            if self.options.max_amount:
                if numit >= self.options.max_amount:
                    print >> sys.stderr, "*"
                    break
        return None

    def sanitize_arguments(self):
        return True

    def parse(self, argv=None):
        # Parse Options (Using 2.6 OptParse because of Antonio)
        self.options, self.args = self.parse_args()

        # Clean arguments (checking inconsistencies, etc.)
        retval = self.sanitize_arguments()

        # Return command
        if not retval:
            self.optparse.print_help()
        return self.options

    def run(self, convId):
        retdata = self.get_conversation_by_id(int(convId))
        # This is a fallback, for those simpler functions
        # that do not print or store data themselves
        if not retdata:
            return
        if self.options.output:
            self.printresult(retdata)

requestedID = ""

if __name__ == '__main__':
    twc = TwCollect()
    opts = twc.parse(sys.argv)
    for line in sys.stdin:
        if(line != ''):
            line = line.replace("\n", '')
            # print "Downloading conversation: " + line
            requestedID = line
            twc.run(line)
